Create database ClubAjedrez;
USE ClubAjedrez;
CREATE TABLE Hotel(
  `idHotel` INT NOT NULL,
  `nombre` VARCHAR(50) NULL,
  `direccion` VARCHAR(50) NULL,
  `telefono` VARCHAR(10) NULL,
  PRIMARY KEY (`idHotel`))
ENGINE = InnoDB;
CREATE TABLE Sala(
  `idSala` INT NOT NULL,
  `capacidad` INT NULL,
  `medios` VARCHAR(100) NULL,
  `Hotel_idHotel` INT NOT NULL,
FOREIGN KEY (Hotel_idHotel) REFERENCES Hotel(idHotel),
  PRIMARY KEY (`idSala`))
ENGINE = InnoDB;
CREATE TABLE Arbitro (
  `No` VARCHAR(100) NOT NULL,
  `idArbitro` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`idArbitro`))
ENGINE = InnoDB;
CREATE TABLE Partida (
  `idPartida` INT NOT NULL,
  `jornada` VARCHAR(50) NULL,
  `entradas` INT NULL,
  `Arbitro_idArbitro` VARCHAR(50) NOT NULL,
  `Sala_idSala` INT NOT NULL,
  `Hotel_idHotel` INT NOT NULL,
FOREIGN KEY (Arbitro_idArbitro) REFERENCES Arbitro(idArbitro),
FOREIGN KEY (Sala_idSala) REFERENCES Sala(idSala),
FOREIGN KEY (Hotel_idHotel) REFERENCES Hotel(idHotel),
  PRIMARY KEY (`idPartida`))
ENGINE = InnoDB;
CREATE TABLE Pais (
  `idPais` INT NOT NULL,
  `nombre` VARCHAR(50) NULL,
  `numeroClubs` VARCHAR(50) NULL,
  `representa` INT NULL,
  PRIMARY KEY (`idPais`))
ENGINE = InnoDB;
CREATE TABLE Participante (
  `nSocio` VARCHAR(50) NOT NULL,
  `nombre` VARCHAR(50) NULL,
  `direccion` VARCHAR(50) NULL,
  `Pais_idPais` INT NOT NULL,
FOREIGN KEY (Pais_idPais) REFERENCES Pais(idPais),
     PRIMARY KEY (`nSocio`))
ENGINE = InnoDB;
CREATE TABLE Reserva (
  `idReserva` INT NOT NULL,
  `fechaEntrada` VARCHAR(50) NULL,
  `fechaSalida` VARCHAR(50) NULL,
  `Participante_nSocio` VARCHAR(50) NOT NULL,
  `Hotel_idHotel` INT NOT NULL,
FOREIGN KEY (Participante_nSocio) REFERENCES Participante (nSocio),
FOREIGN KEY (Hotel_idHotel) REFERENCES Hotel (idHotel),
PRIMARY KEY (`idReserva`))
ENGINE = InnoDB;
CREATE TABLE Jugador (
  `idJugador` VARCHAR(50) NOT NULL,
  `nivel` INT NULL,
  PRIMARY KEY (`idJugador`))
ENGINE = InnoDB;
CREATE TABLE Juego (
`idJuego` INT NOT NULL,
  `color` VARCHAR(50) NULL,
  `Jugador_idJugador` VARCHAR(50) NOT NULL,
  `Partida_idPartida` INT NOT NULL,
FOREIGN KEY (Jugador_idJugador) REFERENCES Jugador (idJugador),
FOREIGN KEY (Partida_idPartida) REFERENCES Partida (idPartida),
PRIMARY KEY (`idJuego`))
ENGINE = InnoDB;
CREATE TABLE Movimiento (
  `idMovimiento` INT NOT NULL,
  `movimiento` VARCHAR(50) NULL,
  `comentario` VARCHAR(50) NULL,
  `Partida_idPartida` INT NOT NULL,
FOREIGN KEY (Partida_idPartida) REFERENCES Partida (idPartida),
PRIMARY KEY (`idMovimiento`, `Partida_idPartida`))
ENGINE = InnoDB;
CREATE TABLE Campeonato (
  `idCampeonato` INT NOT NULL,
  `nombre` VARCHAR(50) NULL,
  `tipo` VARCHAR(50) NULL,
  `Participante_nSocio` VARCHAR(50) NOT NULL,
FOREIGN KEY (Participante_nSocio) REFERENCES Participante (nSocio),
PRIMARY KEY (`idCampeonato`))
ENGINE = InnoDB;