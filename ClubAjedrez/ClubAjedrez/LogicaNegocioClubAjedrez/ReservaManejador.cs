﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesClubAjedrez;
using AccesoDatosClubAjedrez;
namespace LogicaNegocioClubAjedrez
{
    public class ReservaManejador
    {
        ReservaAccesoDatos _ReservaAccesoDatos = new ReservaAccesoDatos();

        private bool ValidarExpr(string dato)
        {
            //operacion

            return true;

        }

        public Tuple<bool, string> ValidarReserva(Reserva reserva)
        {
            bool error = true;
            string cadenaErrores = "";

            if (reserva.IdReserva.Length == 0 || reserva.IdReserva == null)
            {
                cadenaErrores = cadenaErrores + "* El campo IdReserva no puede ser vacio \n";
                error = false;
            }
            if (reserva.FechaEntrada.Length == 0 || reserva.FechaEntrada == null)
            {
                cadenaErrores = cadenaErrores + "* El campo FechaEntrada no puede ser vacio \n";
                error = false;
            }
            if (reserva.FechaSalida.Length == 0 || reserva.FechaSalida == null)
            {
                cadenaErrores = cadenaErrores + "* El campo FechaSalida no puede ser vacio \n";
                error = false;
            }
            if (reserva.Participante_nSocio1.Length == 0 || reserva.Participante_nSocio1 == null)
            {
                cadenaErrores = cadenaErrores + "* El campo Participante_nSocio no puede ser vacio \n";
                error = false;
            }
            if (reserva.Hotel_idHotel1.Length == 0 || reserva.Hotel_idHotel1 == null)
            {
                cadenaErrores = cadenaErrores + "* El campo Hotel_idHotel no puede ser vacio \n";
                error = false;
            }
            var valida = new Tuple<bool, string>(error, cadenaErrores);
            return valida;

        }

        public void GuardarReserva(Reserva reserva)
        {
            try
            {
                _ReservaAccesoDatos.GuardarReserva(reserva);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }


        }

        public void ActualizarReserva(Reserva reserva)
        {

            try
            {
                _ReservaAccesoDatos.ActualizarReserva(reserva);

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }


        }

        public void EliminarReserva(string reserva)
        {
            try
            {
                _ReservaAccesoDatos.EliminarReserva(reserva);

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }

        }

        public List<Reserva> ObtenerReserva(string consulta)
        {

            var listaReserva = _ReservaAccesoDatos.ObtenerReserva(consulta);

            return listaReserva;

        }
    }
}
