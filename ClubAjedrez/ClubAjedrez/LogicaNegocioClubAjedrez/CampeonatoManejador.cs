﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatosClubAjedrez;
using EntidadesClubAjedrez;
namespace LogicaNegocioClubAjedrez
{
    public class CampeonatoManejador
    {
        CampeonatoAccesoDatos _CampeonatoAccesoDatos = new CampeonatoAccesoDatos();

        private bool ValidarExpr(string dato)
        {
            //operacion

            return true;

        }

        public Tuple<bool, string> ValidarCampeonato(Campeonato campeonato)
        {
            bool error = true;
            string cadenaErrores = "";

            if (campeonato.IdCampeonato.Length == 0 || campeonato.IdCampeonato == null)
            {
                cadenaErrores = cadenaErrores + "* El campo idCampeonato no puede ser vacio \n";
                error = false;
            }
            if (campeonato.Nombre.Length == 0 || campeonato.Nombre == null)
            {
                cadenaErrores = cadenaErrores + "* El campo Nombre no puede ser vacio \n";
                error = false;
            }
            if (campeonato.Tipo.Length == 0 || campeonato.Tipo == null)
            {
                cadenaErrores = cadenaErrores + "* El campo Tipo no puede ser vacio \n";
                error = false;
            }
            if (campeonato.Participante_nSocio1.Length == 0 || campeonato.Participante_nSocio1 == null)
            {
                cadenaErrores = cadenaErrores + "* El campo Participante_nSocio no puede ser vacio \n";
                error = false;
            }
            var valida = new Tuple<bool, string>(error, cadenaErrores);
            return valida;

        }

        public void GuardarCampeonato(Campeonato campeonato)
        {
            try
            {
                _CampeonatoAccesoDatos.GuardarCampeonato(campeonato);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }


        }

        public void ActualizarCampeonato(Campeonato campeonato)
        {

            try
            {
                _CampeonatoAccesoDatos.ActualizarCampeonato(campeonato);

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }


        }

        public void EliminarCampeonato(string campeonato)
        {
            try
            {
                _CampeonatoAccesoDatos.EliminarCampeonato(campeonato);

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }

        }

        public List<Campeonato> ObtenerCampeonato(string consulta)
        {

            var listaCampeonato = _CampeonatoAccesoDatos.ObtenerCampeonato(consulta);

            return listaCampeonato;

        }
    }
}
