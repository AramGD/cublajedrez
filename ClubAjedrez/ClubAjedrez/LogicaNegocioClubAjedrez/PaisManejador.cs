﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatosClubAjedrez;
using EntidadesClubAjedrez;

namespace LogicaNegocioClubAjedrez
{
    public class PaisManejador
    {
        PaisAccesoDatos _PaisAccesoDatos = new PaisAccesoDatos();

        private bool ValidarExpr(string dato)
        {
            //operacion

            return true;

        }

        public Tuple<bool, string> ValidarPais(Pais pais)
        {
            bool error = true;
            string cadenaErrores = "";

            if (pais.IdPais.Length == 0 || pais.IdPais == null)
            {
                cadenaErrores = cadenaErrores + "* El campo IdPais no puede ser vacio \n";
                error = false;
            }
            if (pais.Nombre.Length == 0 || pais.Nombre == null)
            {
                cadenaErrores = cadenaErrores + "* El campo Nombre no puede ser vacio \n";
                error = false;
            }
            if (pais.NumeroClubs.Length == 0 || pais.NumeroClubs == null)
            {
                cadenaErrores = cadenaErrores + "* El campo NumeroClubs no puede ser vacio \n";
                error = false;
            }
            if (pais.Representa.Length == 0 || pais.Representa == null)
            {
                cadenaErrores = cadenaErrores + "* El campo Representa no puede ser vacio \n";
                error = false;
            }
            var valida = new Tuple<bool, string>(error, cadenaErrores);
            return valida;

        }

        public void GuardarPais(Pais pais)
        {
            try
            {
                _PaisAccesoDatos.GuardarPais(pais);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }


        }

        public void ActualizarPais(Pais pais)
        {

            try
            {
                _PaisAccesoDatos.ActualizarPais(pais);

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }


        }

        public void EliminarPais(string pais)
        {
            try
            {
                _PaisAccesoDatos.EliminarPais(pais);

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }

        }

        public List<Pais> ObtenerPais(string consulta)
        {

            var listaPais = _PaisAccesoDatos.ObtenerPais(consulta);

            return listaPais;

        }
    }
}
