﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesClubAjedrez;
using AccesoDatosClubAjedrez;
namespace LogicaNegocioClubAjedrez
{
    public class PartidaManejador
    {
        PartidaAccesoDatos _PartidaAccesoDatos = new PartidaAccesoDatos();

        private bool ValidarExpr(string dato)
        {

            return true;

        }

        public Tuple<bool, string> ValidarPartida(Partida partida)
        {
            bool error = true;
            string cadenaErrores = "";

            if (partida.IdPartida.Length == 0 || partida.IdPartida == null)
            {
                cadenaErrores = cadenaErrores + "* El campo IdPartida no puede ser vacio \n";
                error = false;
            }
            if (partida.Jornada.Length == 0 || partida.Jornada == null)
            {
                cadenaErrores = cadenaErrores + "* El campo Jornada no puede ser vacio \n";
                error = false;
            }
            if (partida.Entrada.Length == 0 || partida.Entrada == null)
            {
                cadenaErrores = cadenaErrores + "* El campo Entrada no puede ser vacio \n";
                error = false;
            }
            //if (partida.Arbitro_idArbitro1.Length == 0 || partida.Arbitro_idArbitro1 == null)
            //{
            //    cadenaErrores = cadenaErrores + "* El campo Arbitro_idArbitro no puede ser vacio \n";
            //    error = false;
            //}
            if (partida.Sala_idSala1.Length == 0 || partida.Sala_idSala1 == null)
            {
                cadenaErrores = cadenaErrores + "* El campo Sala_idSala no puede ser vacio \n";
                error = false;
            }
            if (partida.Hotel_idHotel1.Length == 0 || partida.Hotel_idHotel1 == null)
            {
                cadenaErrores = cadenaErrores + "* El campo Hotel_idHotel no puede ser vacio \n";
                error = false;
            }
            var valida = new Tuple<bool, string>(error, cadenaErrores);
            return valida;

        }

        public void GuardarPartida(Partida partida)
        {
            try
            {
                _PartidaAccesoDatos.GuardarPartida(partida);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }


        }

        public void ActualizarPartida(Partida partida)
        {

            try
            {
                _PartidaAccesoDatos.ActualizarPartida(partida);

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }


        }

        public void EliminarPartida(string partida)
        {
            try
            {
                _PartidaAccesoDatos.EliminarPartida(partida);

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
            
        }

        public List<Partida> ObtenerPartida(string consulta)
        {

            var listaPartida = _PartidaAccesoDatos.ObtenerPartida(consulta);

            return listaPartida;

        }
    }
}
