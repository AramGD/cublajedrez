﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatosClubAjedrez;
using EntidadesClubAjedrez;
namespace LogicaNegocioClubAjedrez
{
    public class ArbitroManejador
    { 
         ArbitroAccesoDatos _ArbitroAccesoDatos = new ArbitroAccesoDatos();

        private bool ValidarExpr(string dato)
        {
            //operacion

            return true;

        }

        public Tuple<bool, string> ValidarArbitro(Arbitro arbitro)
        {
            bool error = true;
            string cadenaErrores = "";
            if (arbitro.No1.Length == 0 || arbitro.No1 == null)
            {
                cadenaErrores = cadenaErrores + "* El campo No no puede ser vacio \n";
                error = false;
            }

            if (arbitro.IdArbitro.Length == 0 || arbitro.IdArbitro == null)
            {
                cadenaErrores = cadenaErrores + "* El campo idArbitro no puede ser vacio \n";
                error = false;
            }
            var valida = new Tuple<bool, string>(error, cadenaErrores);
            return valida;
        }

        public void GuardarArbitro(Arbitro arbitro)
        {
            try
            {
                _ArbitroAccesoDatos.GuardarArbitro(arbitro);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }


        }

        public void ActualizarArbitro(Arbitro arbitro)
        {

            try
            {
                _ArbitroAccesoDatos.ActualizarArbitro(arbitro);

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }


        }

        public void EliminarArbitro(string arbitro)
        {
            try
            {
                _ArbitroAccesoDatos.EliminarArbitro(arbitro);

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }

        }

        public List<Arbitro> ObtenerArbitro(string consulta)
        {

            var listaArbitro = _ArbitroAccesoDatos.ObtenerArbitro(consulta);

            return listaArbitro;

        }
    }
}
