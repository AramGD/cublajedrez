﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesClubAjedrez;
using AccesoDatosClubAjedrez;
namespace LogicaNegocioClubAjedrez
{
    public class SalaManejador
    {
        SalaAccesoDatos _SalaAccesoDatos = new SalaAccesoDatos();

        private bool ValidarExpr(string dato)
        {
            //operacion

            return true;

        }

        public Tuple<bool, string> ValidarSala(Sala sala)
        {
            bool error = true;
            string cadenaErrores = "";

            if (sala.IdSala.Length == 0 || sala.IdSala == null)
            {
                cadenaErrores = cadenaErrores + "* El campo IdSala no puede ser vacio \n";
                error = false;
            }
            if (sala.Capacidad.Length == 0 || sala.Capacidad == null)
            {
                cadenaErrores = cadenaErrores + "* El campo Capacidad no puede ser vacio \n";
                error = false;
            }
            if (sala.Medios.Length == 0 || sala.Medios == null)
            {
                cadenaErrores = cadenaErrores + "* El campo Medios no puede ser vacio \n";
                error = false;
            }
            if (sala.Hotel_idHotel.Length == 0 || sala.Hotel_idHotel == null)
            {
                cadenaErrores = cadenaErrores + "* El campo Hotel_idHote no puede ser vacio \n";
                error = false;
            }
            var valida = new Tuple<bool, string>(error, cadenaErrores);
            return valida;

        }

        public void GuardarSala(Sala sala)
        {
            try
            {
                _SalaAccesoDatos.GuardarSala(sala);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }


        }

        public void ActualizarSala(Sala sala)
        {

            try
            {
                _SalaAccesoDatos.ActualizarSala(sala);

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }


        }

        public void EliminarSala(string sala)
        {
            try
            {
                _SalaAccesoDatos.EliminarSala(sala);

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }

        }

        public List<Sala> ObtenerSala(string consulta)
        {

            var listaSala = _SalaAccesoDatos.ObtenerSala(consulta);

            return listaSala;

        }
    }
}
