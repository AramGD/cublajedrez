﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesClubAjedrez;
using AccesoDatosClubAjedrez;
namespace LogicaNegocioClubAjedrez
{
    public class JugadorManejador
    {
        JugadorAccesoDatos _JugadorAccesoDatos = new JugadorAccesoDatos();

        private bool ValidarExpr(string dato)
        {
            //operacion

            return true;

        }

        public Tuple<bool, string> ValidarJugador(Jugador jugador)
        {
            bool error = true;
            string cadenaErrores = "";

            if (jugador.IdJugador.Length == 0 || jugador.IdJugador == null)
            {
                cadenaErrores = cadenaErrores + "* El campo IdJugador no puede ser vacio \n";
                error = false;
            }
            if (jugador.Nivel.Length == 0 || jugador.Nivel == null)
            {
                cadenaErrores = cadenaErrores + "* El campo Nivel no puede ser vacio \n";
                error = false;
            }
            var valida = new Tuple<bool, string>(error, cadenaErrores);
            return valida;

        }

        public void GuardarJugador(Jugador jugador)
        {
            try
            {
                _JugadorAccesoDatos.GuardarJugador(jugador);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }


        }

        public void ActualizarJugador(Jugador jugador)
        {

            try
            {
                _JugadorAccesoDatos.ActualizarJugador(jugador);

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }


        }

        public void EliminarJugador(string jugador)
        {
            try
            {
                _JugadorAccesoDatos.EliminarJugador(jugador);

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }

        }

        public List<Jugador> ObtenerJugador(string consulta)
        {

            var listaJugador = _JugadorAccesoDatos.ObtenerJugador(consulta);

            return listaJugador;

        }
    }
}
