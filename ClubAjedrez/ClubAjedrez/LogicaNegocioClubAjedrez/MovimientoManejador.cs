﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesClubAjedrez;
using AccesoDatosClubAjedrez;
namespace LogicaNegocioClubAjedrez
{
    public class MovimientoManejador
    {
        MovimientosAccesoDatos _MovimientosAccesoDatos = new MovimientosAccesoDatos();

        private bool ValidarExpr(string dato)
        {
            //operacion

            return true;

        }

        public Tuple<bool, string> ValidarMovimiento(Movimiento movimiento)
        {
            bool error = true;
            string cadenaErrores = "";

            if (movimiento.IdMovimiento.Length == 0 || movimiento.IdMovimiento == null)
            {
                cadenaErrores = cadenaErrores + "* El campo IdMovimiento no puede ser vacio \n";
                error = false;
            }
            if (movimiento.Movimientos1.Length == 0 || movimiento.Movimientos1 == null)
            {
                cadenaErrores = cadenaErrores + "* El campo Movimientos no puede ser vacio \n";
                error = false;
            }
            if (movimiento.Comentario.Length == 0 || movimiento.Comentario == null)
            {
                cadenaErrores = cadenaErrores + "* El campo Comentario no puede ser vacio \n";
                error = false;
            }
            if (movimiento.Partida_idPartida1.Length == 0 || movimiento.Partida_idPartida1 == null)
            {
                cadenaErrores = cadenaErrores + "* El campo Partida_idPartida no puede ser vacio \n";
                error = false;
            }
            var valida = new Tuple<bool, string>(error, cadenaErrores);
            return valida;

        }

        public void GuardarMovimiento(Movimiento movimiento)
        {
            try
            {
                _MovimientosAccesoDatos.GuardarMovimiento(movimiento);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }


        }

        public void ActualizarMovimiento(Movimiento movimiento)
        {

            try
            {
                _MovimientosAccesoDatos.ActualizarMovimiento(movimiento);

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }


        }

        public void EliminarMovimiento(string movimiento)
        {
            try
            {
                _MovimientosAccesoDatos.EliminarMovimiento(movimiento);

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }

        }

        public List<Movimiento> ObtenerMovimiento(string consulta)
        {

            var listaMovimiento = _MovimientosAccesoDatos.ObtenerMovimiento(consulta);

            return listaMovimiento;

        }
    }
}
