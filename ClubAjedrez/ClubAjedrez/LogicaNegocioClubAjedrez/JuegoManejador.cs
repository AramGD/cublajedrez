﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatosClubAjedrez;
using EntidadesClubAjedrez;
namespace LogicaNegocioClubAjedrez
{
    public class JuegoManejador
    {
        JuegoAccesoDatos _JuegoAccesoDatos = new JuegoAccesoDatos();

        private bool ValidarExpr(string dato)
        {
            //operacion

            return true;

        }

        public Tuple<bool, string> ValidarJuego(Juego juego)
        {
            bool error = true;
            string cadenaErrores = "";

            if (juego.IdJuego.Length == 0 || juego.IdJuego == null)
            {
                cadenaErrores = cadenaErrores + "* El campo IdJuego no puede ser vacio \n";
                error = false;
            }
            if (juego.Color1.Length == 0 || juego.Color1 == null)
            {
                cadenaErrores = cadenaErrores + "* El campo Color no puede ser vacio \n";
                error = false;
            }
            if (juego.Jugador_idJugador1.Length == 0 || juego.Jugador_idJugador1 == null)
            {
                cadenaErrores = cadenaErrores + "* El campo Jugador_idJugador1 no puede ser vacio \n";
                error = false;
            }
            if (juego.Partida_idPartida1.Length == 0 || juego.Partida_idPartida1 == null)
            {
                cadenaErrores = cadenaErrores + "* El campo Partida_idPartida1 no puede ser vacio \n";
                error = false;
            }
            var valida = new Tuple<bool, string>(error, cadenaErrores);
            return valida;

        }

        public void GuardarJuego(Juego juego)
        {
            try
            {
                _JuegoAccesoDatos.GuardarJuego(juego);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }


        }

        public void ActualizarJuego(Juego juego)
        {

            try
            {
                _JuegoAccesoDatos.ActualizarJuego(juego);

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }


        }

        public void EliminarJuego(string juego)
        {
            try
            {
                _JuegoAccesoDatos.EliminarJuego(juego);

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }

        }

        public List<Juego> ObtenerJuego(string consulta)
        {

            var listaJuego = _JuegoAccesoDatos.ObtenerJuego(consulta);

            return listaJuego;

        }
    }
}
