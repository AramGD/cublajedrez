﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatosClubAjedrez;
using EntidadesClubAjedrez;
namespace LogicaNegocioClubAjedrez
{
    public class HotelManejador
    {
        HotelAccesoDatos _HotelAccesoDatos = new HotelAccesoDatos();

        private bool ValidarExpr(string dato)
        {
            //operacion

            return true;

        }

        public Tuple<bool, string> ValidarHotel(Hotel hotel)
        {
            bool error = true;
            string cadenaErrores = "";

            if (hotel.IdHotel.Length == 0 || hotel.IdHotel == null)
            {
                cadenaErrores = cadenaErrores + "* El campo idHotel no puede ser vacio \n";
                error = false;
            }
            if (hotel.Nombre.Length == 0 || hotel.Nombre == null)
            {
                cadenaErrores = cadenaErrores + "* El campo Nombre no puede ser vacio \n";
                error = false;
            }
            if (hotel.Direccion.Length == 0 || hotel.Direccion == null)
            {
                cadenaErrores = cadenaErrores + "* El campo Direccion no puede ser vacio \n";
                error = false;
            }
            if (hotel.Telefono.Length == 0 || hotel.Telefono == null)
            {
                cadenaErrores = cadenaErrores + "* El campo Telefono no puede ser vacio \n";
                error = false;
            }
            var valida = new Tuple<bool, string>(error, cadenaErrores);
            return valida;

        }

        public void GuardarHotel(Hotel hotel)
        {
            try
            {
                _HotelAccesoDatos.GuardarHotel(hotel);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }


        }

        public void ActualizarHotel(Hotel hotel)
        {

            try
            {
                _HotelAccesoDatos.ActualizarHotel(hotel);

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }


        }

        public void EliminarHotel(string hotel)
        {
            try
            {
                _HotelAccesoDatos.EliminarHotel(hotel);

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }

        }

        public List<Hotel> ObtenerHotel(string consulta)
        {

            var listaHotel = _HotelAccesoDatos.ObtenerHotel(consulta);

            return listaHotel;

        }
    }
}
