﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesClubAjedrez;
using AccesoDatosClubAjedrez;
namespace LogicaNegocioClubAjedrez
{
    public class ParticipanteManejador
    {
        ParticipanteAccesoDatos _ParticipanteAccesoDatos = new ParticipanteAccesoDatos();

        private bool ValidarExpr(string dato)
        {
            //operacion

            return true;

        }

        public Tuple<bool, string> ValidarParticipante(Participante participante)
        {
            bool error = true;
            string cadenaErrores = "";

            if (participante.NSocio.Length == 0 || participante.NSocio == null)
            {
                cadenaErrores = cadenaErrores + "* El campo NSocio no puede ser vacio \n";
                error = false;
            }
            if (participante.Nombre.Length == 0 || participante.Nombre == null)
            {
                cadenaErrores = cadenaErrores + "* El campo Nombre no puede ser vacio \n";
                error = false;
            }
            if (participante.Direccion.Length == 0 || participante.Direccion == null)
            {
                cadenaErrores = cadenaErrores + "* El campo Direccion no puede ser vacio \n";
                error = false;
            }
            if (participante.Pais_idPais1.Length == 0 || participante.Pais_idPais1 == null)
            {
                cadenaErrores = cadenaErrores + "* El campo Pais_idPais no puede ser vacio \n";
                error = false;
            }
            var valida = new Tuple<bool, string>(error, cadenaErrores);
            return valida;

        }

        public void GuardarParticipante(Participante participante)
        {
            try
            {
                _ParticipanteAccesoDatos.GuardarParticipante(participante);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }


        }

        public void ActualizarParticipante(Participante participante)
        {

            try
            {
                _ParticipanteAccesoDatos.ActualizarParticipante(participante);

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }


        }

        public void EliminarParticipante(string participante)
        {
            try
            {
                _ParticipanteAccesoDatos.EliminarParticipante(participante);

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }

        }

        public List<Participante> ObtenerParticipante(string consulta)
        {

            var listaParticipante = _ParticipanteAccesoDatos.ObtenerParticipante(consulta);

            return listaParticipante;

        }
    }
}
