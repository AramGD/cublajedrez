﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesClubAjedrez;
using System.Data;

namespace AccesoDatosClubAjedrez
{
    public class PartidaAccesoDatos
    {
        ConexionAccesoDatos _conexionPartida;
        public PartidaAccesoDatos()
        {
            try
            {
                _conexionPartida = new ConexionAccesoDatos("localhost", "root", "", "ClubAjedrez", 3306);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Fallo la conexion" + ex.Message);
            }
        }
        public void GuardarPartida(Partida partida)
        {
            try
            {
                string consulta = string.Format("insert into Partida values({0}, '{1}' , {2}, '{3}', {4}, {5})", partida.IdPartida, partida.Jornada,
                    partida.Entrada, partida.Arbitro_idArbitro1, partida.Sala_idSala1, partida.Hotel_idHotel1);
                _conexionPartida.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló el guardado " + ex.Message);
            }
        }
        public void ActualizarPartida(Partida partida)
        {

            try
            {
                string consulta = string.Format("update Partida set jornada = '{0}', entradas = {1}, Arbitro_idArbitro = '{2}', Sala_idSala = {3}, Hotel_idHotel = {4}" +
                    " where idPartida = {5}",
                    partida.Jornada, partida.Entrada, partida.Arbitro_idArbitro1, partida.Sala_idSala1, partida.Hotel_idHotel1, partida.IdPartida);
                _conexionPartida.EjecutarConsulta(consulta);

            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló actualizar " + ex.Message);
            }
        }
        public void EliminarPartida(string partida)
        {
            try
            {
                string consulta = string.Format("delete from Partida where idPartida = {0}", partida);
                _conexionPartida.EjecutarConsulta(consulta);

            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la eliminación " + ex.Message);
            }
        }
        public List<Partida> ObtenerPartida(string filtro)
        {

            var listaPartida = new List<Partida>();

            var ds = new DataSet();
            string consulta = string.Format("select * from Partida where jornada like '%{0}%'", filtro);
            ds = _conexionPartida.ObtenerDatos(consulta, "Partida");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var partida = new Partida
                {
                    IdPartida = row["idPartida"].ToString(),
                    Jornada = row["jornada"].ToString(),
                    Entrada = row["entradas"].ToString(),
                    Arbitro_idArbitro1 = row["Arbitro_idArbitro"].ToString(),
                    Sala_idSala1 = row["Sala_idSala"].ToString(),
                    Hotel_idHotel1 = row["Hotel_idHotel"].ToString()
                };

                listaPartida.Add(partida);
            }
            return listaPartida;
        }
    }
}
