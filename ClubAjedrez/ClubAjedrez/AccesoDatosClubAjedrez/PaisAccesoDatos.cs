﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using EntidadesClubAjedrez;


namespace AccesoDatosClubAjedrez
{
    public class PaisAccesoDatos
    {
        ConexionAccesoDatos _conexionPais;
        public PaisAccesoDatos()
        {
            try
            {
                _conexionPais = new ConexionAccesoDatos("localhost", "root", "", "ClubAjedrez", 3306);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la conexión " + ex.Message);
            }
        }
        public void GuardarPais(Pais pais)
        {
            try
            {
                string consulta = string.Format("insert into Pais values({0},'{1}', '{2}', {3})", pais.IdPais, pais.Nombre,
                    pais.NumeroClubs, pais.Representa);
                _conexionPais.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló el guardado " + ex.Message);
            }
        }
        public void ActualizarPais(Pais pais)
        {

            try
            {
                string consulta = string.Format("update Pais set nombre = '{0}', numeroClubs = '{1}', " +
                   "representa = {2} where idPais = {3}",
                    pais.Nombre, pais.NumeroClubs, pais.Representa, pais.IdPais);
                _conexionPais.EjecutarConsulta(consulta);

            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló actualizar " + ex.Message);
            }
        }
        public void EliminarPais(string pais)
        {
            try
            {
                string consulta = string.Format("delete from Pais where idPais = {0}", pais);
                _conexionPais.EjecutarConsulta(consulta);

            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la eliminación " + ex.Message);
            }
        }
        public List<Pais> ObtenerPais(string filtro)
        {

            var listaPais = new List<Pais>();

            var ds = new DataSet();
            string consulta = string.Format("select * from Pais where nombre like '%{0}%'", filtro);
            ds = _conexionPais.ObtenerDatos(consulta, "Pais");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var pais = new Pais
                {
                    IdPais = row["idPais"].ToString(),
                    Nombre = row["nombre"].ToString(),
                    NumeroClubs = row["numeroClubs"].ToString(),
                    Representa = row["representa"].ToString()
                };

                listaPais.Add(pais);
            }
            return listaPais;
        }
    }
}
