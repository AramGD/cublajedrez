﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesClubAjedrez;
using System.Data;

namespace AccesoDatosClubAjedrez
{
    public class ArbitroAccesoDatos
    {
        ConexionAccesoDatos _conexionArbitro;
        public ArbitroAccesoDatos()
        {
            try
            {
                _conexionArbitro = new ConexionAccesoDatos("localhost", "root", "", "ClubAjedrez", 3306);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la conexión " + ex.Message);
            }
        }
        public void GuardarArbitro(Arbitro arbitro)
        {
            try
            {
                string consulta = string.Format("insert into Arbitro values('{0}', '{1}')",arbitro.No1, arbitro.IdArbitro);
                _conexionArbitro.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló el guardado " + ex.Message);
            }
        }
        public void ActualizarArbitro(Arbitro arbitro)
        {

            try
            {
                string consulta = string.Format("update Arbitro set idArbitro = '{0}' where No = '{1}'", arbitro.IdArbitro, arbitro.No1);
                _conexionArbitro.EjecutarConsulta(consulta);

            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló actualizar " + ex.Message);
            }
        }
        public void EliminarArbitro(string arbitro)
        {
            try
            {
                string consulta = string.Format("delete from Arbitro where idArbitro = '{0}'", arbitro);
                _conexionArbitro.EjecutarConsulta(consulta);

            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la eliminación " + ex.Message);
            }
        }
        public List<Arbitro> ObtenerArbitro(string filtro)
        {

            var listaArbitro = new List<Arbitro>();

            var ds = new DataSet();
            string consulta = string.Format("select * from Arbitro where idArbitro like '%{0}%'", filtro);
            ds = _conexionArbitro.ObtenerDatos(consulta, "Arbitro");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var arbitro = new Arbitro
                {
                    No1 = row["No"].ToString(),
                    IdArbitro = row["idArbitro"].ToString()
                };

                listaArbitro.Add(arbitro);
            }
            return listaArbitro;
        }
    }
}
