﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesClubAjedrez;
using System.Data;

namespace AccesoDatosClubAjedrez
{
    public class ParticipanteAccesoDatos
    {
        ConexionAccesoDatos _conexionParticipante;
        public ParticipanteAccesoDatos()
        {
            try
            {
                _conexionParticipante = new ConexionAccesoDatos("localhost", "root", "", "ClubAjedrez", 3306);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la conexión " + ex.Message);
            }
        }
        public void GuardarParticipante(Participante participante)
        {
            try
            {
                string consulta = string.Format("insert into Participante values('{0}', '{1}', '{2}', {3})", participante.NSocio, participante.Nombre, participante.Direccion, participante.Pais_idPais1);
                _conexionParticipante.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló el guardado " + ex.Message);
            }
        }
        public void ActualizarParticipante(Participante participante)
        {

            try
            {
                string consulta = string.Format("update Participante set nombre = '{0}', direccion = '{1}', Pais_idPais = {2} where nSocio = '{3}'",
                    participante.Nombre, participante.Direccion, participante.Pais_idPais1, participante.NSocio);
                _conexionParticipante.EjecutarConsulta(consulta);

            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló actualizar " + ex.Message);
            }
        }
        public void EliminarParticipante(string participante)
        {
            try
            {
                string consulta = string.Format("delete from Participante where nSocio = '{0}'", participante);
                _conexionParticipante.EjecutarConsulta(consulta);

            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la eliminación " + ex.Message);
            }
        }
        public List<Participante> ObtenerParticipante(string filtro)
        {

            var listaParticipante = new List<Participante>();

            var ds = new DataSet();
            string consulta = string.Format("select * from Participante where nSocio like '%{0}%'", filtro);
            ds = _conexionParticipante.ObtenerDatos(consulta, "Participante");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var participante = new Participante
                {
                    NSocio = row["nSocio"].ToString(),
                    Nombre = row["nombre"].ToString(),
                    Direccion = row["direccion"].ToString(),
                    Pais_idPais1 = row["Pais_idPais"].ToString()
                };

                listaParticipante.Add(participante);
            }
            return listaParticipante;
        }
    }
}
