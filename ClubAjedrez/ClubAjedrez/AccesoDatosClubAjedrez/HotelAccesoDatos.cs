﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using EntidadesClubAjedrez;

namespace AccesoDatosClubAjedrez
{
   public class HotelAccesoDatos
    {
        ConexionAccesoDatos _conexionHotel;
        public HotelAccesoDatos()
        {
            try
            {
                _conexionHotel = new ConexionAccesoDatos("localhost", "root", "", "ClubAjedrez", 3306);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la conexión " + ex.Message);
            }
        }
        public void GuardarHotel(Hotel hotel)
        {
            try
            {
                string consulta = string.Format("insert into Hotel values({0},'{1}', '{2}', '{3}')", hotel.IdHotel, hotel.Nombre, hotel.Direccion, hotel.Telefono);
                _conexionHotel.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló el guardado " + ex.Message);
            }
        }
        public void ActualizarHotel(Hotel hotel)
        {

            try
            {
                string consulta = string.Format("update Hotel set nombre = '{0}', direccion = '{1}', telefono = '{2}' where idHotel = {3}",
                    hotel.Nombre, hotel.Direccion, hotel.Telefono, hotel.IdHotel);
                _conexionHotel.EjecutarConsulta(consulta);

            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló actualizar " + ex.Message);
            }
        }
        public void EliminarHotel(string hotel)
        {
            try
            {
                string consulta = string.Format("delete from Hotel where idHotel = {0}", hotel);
                _conexionHotel.EjecutarConsulta(consulta);

            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la eliminación " + ex.Message);
            }
        }
        public List<Hotel> ObtenerHotel(string filtro)
        {

            var listaHotel = new List<Hotel>();

            var ds = new DataSet();
            string consulta = string.Format("select * from Hotel where nombre like '%{0}%'", filtro);//nombre like '%{0}%'
            ds = _conexionHotel.ObtenerDatos(consulta, "Hotel");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var hotel = new Hotel
                {
                    IdHotel = row["idHotel"].ToString(),
                    Nombre = row["nombre"].ToString(),
                    Direccion = row["direccion"].ToString(),
                    Telefono = row["telefono"].ToString(),
                };

                listaHotel.Add(hotel);
            }
            return listaHotel;
        }
    }
}
