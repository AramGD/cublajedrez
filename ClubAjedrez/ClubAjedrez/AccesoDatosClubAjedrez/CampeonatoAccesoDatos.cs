﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesClubAjedrez;
using System.Data;

namespace AccesoDatosClubAjedrez
{
    public class CampeonatoAccesoDatos
    {
        ConexionAccesoDatos _conexionCampeonato;
        public CampeonatoAccesoDatos()
        {
            try
            {
                _conexionCampeonato = new ConexionAccesoDatos("localhost", "root", "", "ClubAjedrez", 3306);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la conexión " + ex.Message);
            }
        }
        public void GuardarCampeonato(Campeonato campeonato)
        {
            try
            {
                string consulta = string.Format("insert into Campeonato values({0},'{1}', '{2}', '{3}')", campeonato.IdCampeonato, campeonato.Nombre, campeonato.Tipo, campeonato.Participante_nSocio1);
                _conexionCampeonato.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló el guardado " + ex.Message);
            }
        }
        public void ActualizarCampeonato(Campeonato campeonato)
        {

            try
            {
                string consulta = string.Format("update Campeonato set nombre = '{0}', tipo = '{1}', Participante_nSocio = '{2}' where idCampeonato = {3}",
                    campeonato.Nombre, campeonato.Tipo, campeonato.Participante_nSocio1, campeonato.IdCampeonato);
                _conexionCampeonato.EjecutarConsulta(consulta);

            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló actualizar " + ex.Message);
            }
        }
        public void EliminarCampeonato(string campeonato)
        {
            try
            {
                string consulta = string.Format("delete from Campeonato where idCampeonato = {0}", campeonato);
                _conexionCampeonato.EjecutarConsulta(consulta);

            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la eliminación " + ex.Message);
            }
        }
        public List<Campeonato> ObtenerCampeonato(string filtro)
        {

            var listaCampeonato = new List<Campeonato>();

            var ds = new DataSet();
            string consulta = string.Format("select * from Campeonato where nombre like '%{0}%'", filtro);
            ds = _conexionCampeonato.ObtenerDatos(consulta, "Campeonato");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var campeonato = new Campeonato
                {
                    IdCampeonato = row["idCampeonato"].ToString(),
                    Nombre = row["nombre"].ToString(),
                    Tipo = row["tipo"].ToString(),
                    Participante_nSocio1 = row["Participante_nSocio"].ToString()
                };

                listaCampeonato.Add(campeonato);
            }
            return listaCampeonato;
        }
    }
}
