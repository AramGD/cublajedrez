﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesClubAjedrez;
using System.Data;

namespace AccesoDatosClubAjedrez
{
    public class SalaAccesoDatos
    {
        ConexionAccesoDatos _conexionSala;
        public SalaAccesoDatos()
        {
            try
            {
                _conexionSala = new ConexionAccesoDatos("localhost", "root", "", "ClubAjedrez", 3306);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Fallo la conexion" + ex.Message);
            }
        }
        public void GuardarSala(Sala sala)
        {
            try
            {
                string consulta = string.Format("insert into Sala values({0}, {1} , '{2}', {3})", sala.IdSala, sala.Capacidad, sala.Medios, sala.Hotel_idHotel);
                _conexionSala.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló el guardado " + ex.Message);
            }
        }
        public void ActualizarSala(Sala sala)
        {

            try
            {
                string consulta = string.Format("update Sala set capacidad = {0}, medios = '{1}', Hotel_idHotel = {2} where idSala = {3}",
                    sala.Capacidad, sala.Medios, sala.Hotel_idHotel, sala.IdSala);
                _conexionSala.EjecutarConsulta(consulta);

            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló actualizar " + ex.Message);
            }
        }
        public void EliminarSala(string sala)
        {
            try
            {
                string consulta = string.Format("delete from Sala where idSala = {0}", sala);
                _conexionSala.EjecutarConsulta(consulta);

            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la eliminación " + ex.Message);
            }
        }
        public List<Sala> ObtenerSala(string filtro)
        {

            var listaSala = new List<Sala>();

            var ds = new DataSet();
            string consulta = string.Format("select * from Sala where medios like '%{0}%'", filtro);
            ds = _conexionSala.ObtenerDatos(consulta, "Sala");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var sala = new Sala
                {
                    IdSala = row["idSala"].ToString(),
                    Capacidad = row["capacidad"].ToString(),
                    Medios = row["medios"].ToString(),
                    Hotel_idHotel = row["Hotel_idHotel"].ToString()
                };

                listaSala.Add(sala);
            }
            return listaSala;
        }
    }
}
