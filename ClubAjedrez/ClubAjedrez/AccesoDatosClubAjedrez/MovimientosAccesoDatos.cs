﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesClubAjedrez;
using System.Data;

namespace AccesoDatosClubAjedrez
{
    public class MovimientosAccesoDatos
    {
        ConexionAccesoDatos _conexionMovimiento;
        public MovimientosAccesoDatos()
        {
            try
            {
                _conexionMovimiento = new ConexionAccesoDatos("localhost", "root", "", "ClubAjedrez", 3306);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la conexión " + ex.Message);
            }
        }
        public void GuardarMovimiento(Movimiento movimiento)
        {
            try
            {
                string consulta = string.Format("insert into Movimiento values({0},'{1}', '{2}', {3})", movimiento.IdMovimiento, movimiento.Movimientos1,
                    movimiento.Comentario, movimiento.Partida_idPartida1);
                _conexionMovimiento.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló el guardado " + ex.Message);
            }
        }
        public void ActualizarMovimiento(Movimiento movimiento)
        {

            try
            {
                string consulta = string.Format("update Movimiento set movimiento = '{0}', comentario = '{1}', Partida_idPartida = {2} where idMovimiento = {3}",
                    movimiento.Movimientos1, movimiento.Comentario, movimiento.Partida_idPartida1, movimiento.IdMovimiento);
                _conexionMovimiento.EjecutarConsulta(consulta);

            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló actualizar " + ex.Message);
            }
        }
        public void EliminarMovimiento(string movimiento)
        {
            try
            {
                string consulta = string.Format("delete from Movimiento where idMovimiento = {0}", movimiento);
                _conexionMovimiento.EjecutarConsulta(consulta);

            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la eliminación " + ex.Message);
            }
        }
        public List<Movimiento> ObtenerMovimiento(string filtro)
        {

            var listaMovimiento = new List<Movimiento>();

            var ds = new DataSet();
            string consulta = string.Format("select * from Movimiento where movimiento like '%{0}%'", filtro);
            ds = _conexionMovimiento.ObtenerDatos(consulta, "Movimiento");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var movimiento = new Movimiento
                {
                    IdMovimiento = row["idMovimiento"].ToString(),
                    Movimientos1 = row["movimiento"].ToString(),
                    Comentario = row["comentario"].ToString(),
                    Partida_idPartida1 = row["Partida_idPartida"].ToString()
                };

                listaMovimiento.Add(movimiento);
            }
            return listaMovimiento;
        }
    }
}
