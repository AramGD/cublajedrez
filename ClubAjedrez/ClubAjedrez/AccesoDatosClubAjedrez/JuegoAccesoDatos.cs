﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesClubAjedrez;
using System.Data;

namespace AccesoDatosClubAjedrez
{
    public class JuegoAccesoDatos
    {
        ConexionAccesoDatos _conexionJuego;
        public JuegoAccesoDatos()
        {
            try
            {
                _conexionJuego = new ConexionAccesoDatos("localhost", "root", "", "ClubAjedrez", 3306);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la conexión " + ex.Message);
            }
        }
        public void GuardarJuego(Juego juego)
        {
            try
            {
                string consulta = string.Format("insert into Juego values({0}, '{1}', '{2}', {3})", juego.IdJuego, juego.Color1, juego.Jugador_idJugador1, juego.Partida_idPartida1);
                _conexionJuego.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló el guardado " + ex.Message);
            }
        }
        public void ActualizarJuego(Juego juego)
        {

            try
            {
                string consulta = string.Format("update Juego set color = '{0}', Jugador_idJugador = '{1}', Partida_idPartida = {2} where idJuego = {3}",
                   juego.Color1, juego.Jugador_idJugador1, juego.Partida_idPartida1, juego.IdJuego);
                _conexionJuego.EjecutarConsulta(consulta);

            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló actualizar " + ex.Message);
            }
        }
        public void EliminarJuego(string juego)
        {
            try
            {
                string consulta = string.Format("delete from Juego where idJuego = {0}", juego);
                _conexionJuego.EjecutarConsulta(consulta);

            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la eliminación " + ex.Message);
            }
        }
        public List<Juego> ObtenerJuego(string filtro)
        {

            var listaJuego = new List<Juego>();

            var ds = new DataSet();
            string consulta = string.Format("select * from Juego where color like '%{0}%'", filtro);
            ds = _conexionJuego.ObtenerDatos(consulta, "Juego");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var juego = new Juego
                {
                    IdJuego = row["idJuego"].ToString(),
                    Color1 = row["color"].ToString(),
                    Jugador_idJugador1 = row["Jugador_idJugador"].ToString(),
                    Partida_idPartida1 = row["Partida_idPartida"].ToString()
                };

                listaJuego.Add(juego);
            }
            return listaJuego;
        }
    }
}
