﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesClubAjedrez;
using System.Data;

namespace AccesoDatosClubAjedrez
{
    public class ReservaAccesoDatos
    {
        ConexionAccesoDatos _conexionReserva;
        public ReservaAccesoDatos()
        {
            try
            {
                _conexionReserva = new ConexionAccesoDatos("localhost", "root", "", "ClubAjedrez", 3306);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la conexión " + ex.Message);
            }
        }
        public void GuardarReserva(Reserva reserva)
        {
            try
            {
                string consulta = string.Format("insert into Reserva values({0}, '{1}', '{2}', '{3}', {4})", reserva.IdReserva,
                    reserva.FechaEntrada, reserva.FechaSalida, reserva.Participante_nSocio1, reserva.Hotel_idHotel1);
                _conexionReserva.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló el guardado " + ex.Message);
            }
        }
        public void ActualizarReserva(Reserva reserva)
        {

            try
            {
                string consulta = string.Format("update Reserva set fechaEntrada = '{0}', fechaSalida = '{1}', Participante_nSocio = '{2}', Hotel_idHotel = {3} where idReserva = {4}",
                    reserva.FechaEntrada, reserva.FechaSalida, reserva.Participante_nSocio1, reserva.Hotel_idHotel1, reserva.IdReserva);
                _conexionReserva.EjecutarConsulta(consulta);

            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló actualizar " + ex.Message);
            }
        }
        public void EliminarReserva(string reserva)
        {
            try
            {
                string consulta = string.Format("delete from Reserva where idReserva = {0}", reserva);
                _conexionReserva.EjecutarConsulta(consulta);

            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la eliminación " + ex.Message);
            }
        }
        public List<Reserva> ObtenerReserva(string filtro)
        {

            var listaReserva = new List<Reserva>();

            var ds = new DataSet();
            string consulta = string.Format("select * from Reserva where fechaEntrada like '%{0}%'", filtro);
            ds = _conexionReserva.ObtenerDatos(consulta, "Reserva");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var reserva = new Reserva
                {
                    IdReserva = row["idReserva"].ToString(),
                    FechaEntrada = row["fechaEntrada"].ToString(),
                    FechaSalida = row["fechaSalida"].ToString(),
                    Participante_nSocio1 = row["Participante_nSocio"].ToString(),
                    Hotel_idHotel1 = row["Hotel_idHotel"].ToString()
                };

                listaReserva.Add(reserva);
            }
            return listaReserva;
        }
    }
}
