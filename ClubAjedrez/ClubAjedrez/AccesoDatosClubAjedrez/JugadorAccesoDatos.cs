﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using EntidadesClubAjedrez;

namespace AccesoDatosClubAjedrez
{
   public class JugadorAccesoDatos
    {
        ConexionAccesoDatos _conexionJugador;
        public JugadorAccesoDatos()
        {
            try
            {
                _conexionJugador = new ConexionAccesoDatos("localhost", "root", "", "ClubAjedrez", 3306);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la conexión " + ex.Message);
            }
        }
        public void GuardarJugador(Jugador jugador)
        {
            try
            {
                string consulta = string.Format("insert into Jugador values( '{0}',{1})", jugador.IdJugador, jugador.Nivel);
                _conexionJugador.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló el guardado " + ex.Message);
            }
        }
        public void ActualizarJugador(Jugador jugador)
        {
            try
            {
                string consulta = string.Format("update Jugador set nivel = {0} where idJugador = '{1}'",
                    jugador.Nivel, jugador.IdJugador);
                _conexionJugador.EjecutarConsulta(consulta);

            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló actualizar " + ex.Message);
            }
        }

        public void EliminarJugador(string jugador)
        {
            try
            {
                string consulta = string.Format("delete from Jugador where idJugador = '{0}'", jugador);
                _conexionJugador.EjecutarConsulta(consulta);

            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la eliminación " + ex.Message);
            }
        }
        public List<Jugador> ObtenerJugador(string filtro)
        {

            var listaJugador = new List<Jugador>();

            var ds = new DataSet();
            string consulta = string.Format("select * from Jugador where idJugador like '%{0}%'", filtro);
            ds = _conexionJugador.ObtenerDatos(consulta, "Jugador");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var jugador = new Jugador
                {
                    IdJugador = row["idJugador"].ToString(),
                    Nivel = row["nivel"].ToString()
                };

                listaJugador.Add(jugador);
            }
            return listaJugador;
        }
    }
}
