﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocioClubAjedrez;
using EntidadesClubAjedrez;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.text;

namespace ClubAjedrezz
{
    public partial class FrmMovimiento : Form
    {
        #region Variables
        private MovimientoManejador _MovimientoManejador;
        private Movimiento _movimiento;
        private string banderaGuardar;
        #endregion
        #region Constructor
        public FrmMovimiento()
        {
            InitializeComponent();
            _MovimientoManejador = new MovimientoManejador();
            _movimiento = new Movimiento();
        }
        #endregion
        #region Eventos
        private void FrmMovimiento_Load(object sender, EventArgs e)
        {
            Botonera(true, false, false, true);
            BuscarMovimiento("");
            Cuadros(false, false, false, false);
            LimpiarCuadros();
        }
        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Botonera(false, true, true, false);
            Cuadros(true, true, true, true);
            txtIdPartida.Focus();
            banderaGuardar = "guardar";
        }
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (banderaGuardar == "guardar")
            {

                GuardarMovimiento();
                BuscarMovimiento("");


            }
            else
            {
                Actualizar();
                BuscarMovimiento("");

            }

            Botonera(true, false, false, true);
            LimpiarCuadros();
            Cuadros(false, false, false, false);
        }
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Botonera(true, false, false, true);
            LimpiarCuadros();
            Cuadros(false, false, false, false);
        }
        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarMovimiento(txtBuscar.Text);
        }
        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea eliminar el movimiento actual", "Eliminar movimiento", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Eliminar();
                BuscarMovimiento("");
                MessageBox.Show("Movimiento eliminada");
            }
        }
        private void dtvMovimiento_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Botonera(false, true, true, false);
            Cuadros(false, true, true, true);
            txtIdMovimiento.Focus();
            txtIdMovimiento.Text = dtvMovimiento.CurrentRow.Cells["IdMovimiento"].Value.ToString();
            txtMovimiento.Text = dtvMovimiento.CurrentRow.Cells["Movimientos1"].Value.ToString();
            txtComentario.Text = dtvMovimiento.CurrentRow.Cells["Comentario"].Value.ToString();
            txtIdPartida.Text = dtvMovimiento.CurrentRow.Cells["Partida_idPartida1"].Value.ToString();
            banderaGuardar = "actualizar";
        }
        private void btnPdf_Click(object sender, EventArgs e)
        {
            CrearPdf();
        }

        #endregion
        #region Metodos
        private void Botonera(Boolean nuevo, Boolean guardar, Boolean cancelar, Boolean eliminar)
        {
            btnNuevo.Enabled = nuevo;
            btnGuardar.Enabled = guardar;
            btnCancelar.Enabled = cancelar;
            btnEliminar.Enabled = eliminar;

        }
        private void BuscarMovimiento(string consulta)
        {
            dtvMovimiento.DataSource = _MovimientoManejador.ObtenerMovimiento(consulta);

        }
        private void Cuadros(Boolean idMovimiento, Boolean movimiento, Boolean comentario, Boolean idPartida)
        {
            txtIdMovimiento.Enabled = idMovimiento;
            txtMovimiento.Enabled = movimiento;
            txtComentario.Enabled = comentario;
            txtIdPartida.Enabled = idPartida;
        }
        private void LimpiarCuadros()
        {

            txtIdMovimiento.Text = "";
            txtMovimiento.Text = "";
            txtComentario.Text = "";
            txtIdPartida.Text = "";
        }
        private void GuardarMovimiento()
        {
            Movimiento movimiento = new Movimiento();
            movimiento.IdMovimiento = txtIdMovimiento.Text;
            movimiento.Movimientos1 = txtMovimiento.Text;
            movimiento.Comentario = txtComentario.Text;
            movimiento.Partida_idPartida1 = txtIdPartida.Text;

            var valida = _MovimientoManejador.ValidarMovimiento(movimiento);

            if (valida.Item1)
            {
                _MovimientoManejador.GuardarMovimiento(movimiento);
                MessageBox.Show("El movimiento se guardo correctamente");

            }
            else
            {

                MessageBox.Show(valida.Item2, "Ocurrio un error al guardar el movimiento", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }
        private void Actualizar()
        {

            _MovimientoManejador.ActualizarMovimiento(new Movimiento
            {

                IdMovimiento = txtIdMovimiento.Text,
                Movimientos1 = txtMovimiento.Text,
                Comentario = txtComentario.Text,
                Partida_idPartida1 = txtIdPartida.Text

            });

        }
        private void Eliminar()
        {
            var idMovimiento = dtvMovimiento.CurrentRow.Cells["idMovimiento"].Value.ToString();
            _MovimientoManejador.EliminarMovimiento(idMovimiento);

        }
        private void CrearPdf()
        {
            Document documento = new Document(PageSize.A4);
            PdfWriter.GetInstance(documento, new FileStream("C:\\PDF\\reporteMovimiento.pdf", FileMode.OpenOrCreate));

            documento.Open();
            iTextSharp.text.Image logo = iTextSharp.text.Image.GetInstance(@"C:\Users\churr\source\repos\ClubAjedrez\ClubAjedrezz\Resources\icons8-chessboard-96.png");
            logo.SetAbsolutePosition(30, 700);
            logo.ScaleAbsolute(60, 60);
            documento.Add(logo);

            Paragraph titulo = new Paragraph();
            titulo.Alignment = Element.ALIGN_CENTER;
            titulo.Font = FontFactory.GetFont("Arial", 20);
            titulo.Add("Movimiento");
            documento.Add(titulo);

            documento.Add(new Paragraph(" "));
            Paragraph subtitulo = new Paragraph();
            subtitulo.Alignment = 1;
            subtitulo.Add("Reporte de lista de Movimientos");
            documento.Add(subtitulo);
            documento.Add(new Paragraph(" "));

            PdfPTable tabla = new PdfPTable(4);

            PdfPCell tituloTabla = new PdfPCell(new Phrase("Datos Movimientos"));
            float[] medidas = { 2.55f, 2.55f, 2.55f, 2.55f };
            tabla.SetWidths(medidas);
            tituloTabla.Colspan = 4;
            tituloTabla.HorizontalAlignment = 1;
            tabla.AddCell(tituloTabla);

            documento.Add(new Paragraph(" "));

            PdfPCell subTituloidMovimiento = new PdfPCell(new Phrase("idMovimiento"));
            subTituloidMovimiento.HorizontalAlignment = 1;
            tabla.AddCell(subTituloidMovimiento);


            PdfPCell subTituloMovimiento = new PdfPCell(new Phrase("Movimiento"));
            subTituloMovimiento.HorizontalAlignment = 1;
            tabla.AddCell(subTituloMovimiento);


            PdfPCell subTituloComentario = new PdfPCell(new Phrase("Comentario"));
            subTituloComentario.HorizontalAlignment = 1;
            tabla.AddCell(subTituloComentario);


            PdfPCell subTituloIdPartida = new PdfPCell(new Phrase("idPartida"));
            subTituloIdPartida.HorizontalAlignment = 1;
            tabla.AddCell(subTituloIdPartida);

            var listaMovimiento = _MovimientoManejador.ObtenerMovimiento("");
            foreach (var movimiento in listaMovimiento)
            {
                tabla.AddCell(new Phrase(movimiento.IdMovimiento));
                tabla.AddCell(new Phrase(movimiento.Movimientos1));
                tabla.AddCell(new Phrase(movimiento.Comentario));
                tabla.AddCell(new Phrase(movimiento.Partida_idPartida1));
            }

            documento.Add(tabla);
            documento.Close();

            System.Diagnostics.Process.Start(@"C:\\PDF\\reporteMovimiento.pdf");
        }
        #endregion


    }
}
