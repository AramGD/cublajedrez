﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocioClubAjedrez;
using EntidadesClubAjedrez;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.text;

namespace ClubAjedrezz
{
    public partial class FrmPartida : Form
    {
        #region Variables
        private PartidaManejador _PartidaManejador;
        private Partida _partida;
        private string banderaGuardar;
        #endregion
        #region Constructor
        public FrmPartida()
        {
            InitializeComponent();
            _PartidaManejador = new PartidaManejador();
            _partida = new Partida();
        }
        #endregion
        #region Eventos
        private void FrmPartida_Load(object sender, EventArgs e)
        {
            Botonera(true, false, false, true);
            BuscarPartida("");
            Cuadros(false, false, false, false, false, false);
            LimpiarCuadros();
        }
        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Botonera(false, true, true, false);
            Cuadros(true, true, true, true, true, true);
            txtIdPartida.Focus();
            banderaGuardar = "guardar";
        }
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (banderaGuardar == "guardar")
            {

                GuardarPartida();
                BuscarPartida("");


            }
            else
            {
                Actualizar();
                BuscarPartida("");

            }

            Botonera(true, false, false, true);
            LimpiarCuadros();
            Cuadros(false, false, false, false, false, false);
        }
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Botonera(true, false, false, true);
            LimpiarCuadros();
            Cuadros(false, false, false, false, false, false);
        }
        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarPartida(txtBuscar.Text);
        }
        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea eliminar la partida actual", "Eliminar partida", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Eliminar();
                BuscarPartida("");
                MessageBox.Show("Partida eliminada");
            }
        }
        private void dtvPartida_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Botonera(false, true, true, false);
            Cuadros(false, true, true, true, true, true);
            txtIdPartida.Focus();
            txtIdPartida.Text = dtvPartida.CurrentRow.Cells["IdPartida"].Value.ToString();
            txtJornada.Text = dtvPartida.CurrentRow.Cells["Jornada"].Value.ToString();
            txtEntrada.Text = dtvPartida.CurrentRow.Cells["Entrada"].Value.ToString();
            txtIdArbitro.Text = dtvPartida.CurrentRow.Cells["Arbitro_idArbitro1"].Value.ToString();
            txtIdSala.Text = dtvPartida.CurrentRow.Cells["Sala_idSala1"].Value.ToString();
            txtIdHotel.Text = dtvPartida.CurrentRow.Cells["Hotel_idHotel1"].Value.ToString();
            banderaGuardar = "actualizar";
        }
        private void btnPdf_Click(object sender, EventArgs e)
        {
            CrearPdf();
        }
        #endregion
        #region Metodos
        private void Botonera(Boolean nuevo, Boolean guardar, Boolean cancelar, Boolean eliminar)
        {
            btnNuevo.Enabled = nuevo;
            btnGuardar.Enabled = guardar;
            btnCancelar.Enabled = cancelar;
            btnEliminar.Enabled = eliminar;

        }
        private void BuscarPartida(string consulta)
        {
            dtvPartida.DataSource = _PartidaManejador.ObtenerPartida(consulta);

        }
        private void Cuadros(Boolean idPartida, Boolean jornada, Boolean entradas, Boolean idArbitro, Boolean idSala, Boolean idHotel)
        {
            txtIdPartida.Enabled = idPartida;
            txtJornada.Enabled = jornada;
            txtEntrada.Enabled = entradas;
            txtIdArbitro.Enabled = idArbitro;
            txtIdSala.Enabled = idSala;
            txtIdHotel.Enabled = idHotel;
        }
        private void LimpiarCuadros()
        {

            txtIdPartida.Text = "";
            txtJornada.Text = "";
            txtEntrada.Text = "";
            txtIdArbitro.Text = "";
            txtIdSala.Text = "";
            txtIdHotel.Text = "";
        }
        private void GuardarPartida()
        {
            Partida partida = new Partida();
            partida.IdPartida = txtIdPartida.Text;
            partida.Jornada = txtJornada.Text;
            partida.Entrada = txtEntrada.Text;
            partida.Arbitro_idArbitro1 = txtIdArbitro.Text;
            partida.Sala_idSala1 = txtIdSala.Text;
            partida.Hotel_idHotel1 = txtIdHotel.Text;

            var valida = _PartidaManejador.ValidarPartida(partida);

            if (valida.Item1)
            {
                _PartidaManejador.GuardarPartida(partida);
                MessageBox.Show("La partida se guardo correctamente");

            }
            else
            {

                MessageBox.Show(valida.Item2, "Ocurrio un error al guardar la partida", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }
        private void Actualizar()
        {

            _PartidaManejador.ActualizarPartida(new Partida
            {

                IdPartida = txtIdPartida.Text,
                Jornada = txtJornada.Text,
                Entrada = txtEntrada.Text,
                Arbitro_idArbitro1 = txtIdArbitro.Text,
                Sala_idSala1 = txtIdSala.Text,
                Hotel_idHotel1 = txtIdHotel.Text

            });

        }
        private void Eliminar()
        {
            var idPartida = dtvPartida.CurrentRow.Cells["idPartida"].Value.ToString();
            _PartidaManejador.EliminarPartida(idPartida);

        }
        private void CrearPdf()
        {
            Document documento = new Document(PageSize.A4);
            PdfWriter.GetInstance(documento, new FileStream("C:\\PDF\\reportePartida.pdf", FileMode.OpenOrCreate));

            documento.Open();
            iTextSharp.text.Image logo = iTextSharp.text.Image.GetInstance(@"C:\Users\churr\source\repos\ClubAjedrez\ClubAjedrezz\Resources\icons8-chessboard-96.png");
            logo.SetAbsolutePosition(30, 700);
            logo.ScaleAbsolute(60, 60);
            documento.Add(logo);

            Paragraph titulo = new Paragraph();
            titulo.Alignment = Element.ALIGN_CENTER;
            titulo.Font = FontFactory.GetFont("Arial", 20);
            titulo.Add("Partida");
            documento.Add(titulo);

            documento.Add(new Paragraph(" "));
            Paragraph subtitulo = new Paragraph();
            subtitulo.Alignment = 1;
            subtitulo.Add("Reporte de lista de partidas");
            documento.Add(subtitulo);
            documento.Add(new Paragraph(" "));

            PdfPTable tabla = new PdfPTable(6);

            PdfPCell tituloTabla = new PdfPCell(new Phrase("Datos Partidas"));
            float[] medidas = { 2.55f, 2.55f, 2.55f, 2.55f, 2.55f, 2.55f };
            tabla.SetWidths(medidas);
            tituloTabla.Colspan = 6;
            tituloTabla.HorizontalAlignment = 1;
            tabla.AddCell(tituloTabla);

            documento.Add(new Paragraph(" "));

            PdfPCell subTituloidPartida = new PdfPCell(new Phrase("idPartida"));
            subTituloidPartida.HorizontalAlignment = 1;
            tabla.AddCell(subTituloidPartida);


            PdfPCell subTituloJornada = new PdfPCell(new Phrase("jornada"));
            subTituloJornada.HorizontalAlignment = 1;
            tabla.AddCell(subTituloJornada);


            PdfPCell subTituloEntrada = new PdfPCell(new Phrase("entrada"));
            subTituloEntrada.HorizontalAlignment = 1;
            tabla.AddCell(subTituloEntrada);


            PdfPCell subTituloIdArbitro = new PdfPCell(new Phrase("Arbitro_idArbitro"));
            subTituloIdArbitro.HorizontalAlignment = 1;
            tabla.AddCell(subTituloIdArbitro);

            PdfPCell subTituloIdSala = new PdfPCell(new Phrase("Sala_idSala"));
            subTituloIdSala.HorizontalAlignment = 1;
            tabla.AddCell(subTituloIdSala);

            PdfPCell subTituloIdHotel = new PdfPCell(new Phrase("Hotel_idHotel"));
            subTituloIdHotel.HorizontalAlignment = 1;
            tabla.AddCell(subTituloIdHotel);

            var listaPartida = _PartidaManejador.ObtenerPartida("");
            foreach (var partida in listaPartida)
            {
                tabla.AddCell(new Phrase(partida.IdPartida));
                tabla.AddCell(new Phrase(partida.Jornada));
                tabla.AddCell(new Phrase(partida.Entrada));
                tabla.AddCell(new Phrase(partida.Arbitro_idArbitro1));
                tabla.AddCell(new Phrase(partida.Sala_idSala1));
                tabla.AddCell(new Phrase(partida.Hotel_idHotel1));
            }

            documento.Add(tabla);
            documento.Close();

            System.Diagnostics.Process.Start(@"C:\\PDF\\reportePartida.pdf");
        }
        #endregion
    }
}
