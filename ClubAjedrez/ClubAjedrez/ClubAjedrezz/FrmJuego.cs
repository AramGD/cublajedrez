﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocioClubAjedrez;
using EntidadesClubAjedrez;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.text;


namespace ClubAjedrezz
{
    public partial class FrmJuego : Form
    {
        #region Variables
        private JuegoManejador _JuegoManejador;
        private Juego _juego;
        private string banderaGuardar;
        #endregion
        #region Constructor
        public FrmJuego()
        {
            InitializeComponent();
            _JuegoManejador = new JuegoManejador();
            _juego = new Juego();
        }
        #endregion
        #region Eventos
        private void FrmJuego_Load(object sender, EventArgs e)
        {
            Botonera(true, false, false, true);
            BuscarJuego("");
            Cuadros(false, false, false, false);
            LimpiarCuadros();
        }
        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Botonera(false, true, true, false);
            Cuadros(true, true, true, true);
            txtIdJuego.Focus();
            banderaGuardar = "guardar";
        }
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (banderaGuardar == "guardar")
            {

                GuardarJuego();
                BuscarJuego("");


            }
            else
            {
                Actualizar();
                BuscarJuego("");

            }

            Botonera(true, false, false, true);
            LimpiarCuadros();
            Cuadros(false, false, false, false);
        }
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Botonera(true, false, false, true);
            LimpiarCuadros();
            Cuadros(false, false, false, false);
        }
        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarJuego(txtBuscar.Text);
        }
        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea eliminar el juego actual", "Eliminar juego", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Eliminar();
                BuscarJuego("");
                MessageBox.Show("Juego eliminada");
            }
        }
        private void dtvJuego_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Botonera(false, true, true, false);
            Cuadros(false, true, true, true);
            txtIdJuego.Focus();
            txtIdJuego.Text = dtvJuego.CurrentRow.Cells["IdJuego"].Value.ToString();
            txtColor.Text = dtvJuego.CurrentRow.Cells["Color1"].Value.ToString();
            txtIdJugador.Text = dtvJuego.CurrentRow.Cells["Jugador_idJugador1"].Value.ToString();
            txtIdPartida.Text = dtvJuego.CurrentRow.Cells["Partida_idPartida1"].Value.ToString();
            banderaGuardar = "actualizar";
        }
        private void btnPdf_Click(object sender, EventArgs e)
        {
            CrearPdf();
        }

        #endregion
        #region Metodos
        private void Botonera(Boolean nuevo, Boolean guardar, Boolean cancelar, Boolean eliminar)
        {
            btnNuevo.Enabled = nuevo;
            btnGuardar.Enabled = guardar;
            btnCancelar.Enabled = cancelar;
            btnEliminar.Enabled = eliminar;

        }
        private void BuscarJuego(string consulta)
        {
            dtvJuego.DataSource = _JuegoManejador.ObtenerJuego(consulta);

        }
        private void Cuadros(Boolean idJuego, Boolean color, Boolean idJugador, Boolean idPartida)
        {
            txtIdJuego.Enabled = idJuego;
            txtColor.Enabled = color;
            txtIdJugador.Enabled = idJugador;
            txtIdPartida.Enabled = idPartida;
        }
        private void LimpiarCuadros()
        {

            txtIdJuego.Text = "";
            txtColor.Text = "";
            txtIdJugador.Text = "";
            txtIdPartida.Text = "";
        }
        private void GuardarJuego()
        {
            Juego juego = new Juego();
            juego.IdJuego = txtIdJuego.Text;
            juego.Color1 = txtColor.Text;
            juego.Jugador_idJugador1 = txtIdJugador.Text;
            juego.Partida_idPartida1 = txtIdPartida.Text;

            var valida = _JuegoManejador.ValidarJuego(juego);

            if (valida.Item1)
            {
                _JuegoManejador.GuardarJuego(juego);
                MessageBox.Show("El juego se guardo correctamente");

            }
            else
            {

                MessageBox.Show(valida.Item2, "Ocurrio un error al guardar el juego", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }
        private void Actualizar()
        {

            _JuegoManejador.ActualizarJuego(new Juego
            {

                IdJuego = txtIdJuego.Text,
                Color1 = txtColor.Text,
                Jugador_idJugador1 = txtIdJugador.Text,
                Partida_idPartida1 = txtIdPartida.Text

            });

        }
        private void Eliminar()
        {
            var idJuego = dtvJuego.CurrentRow.Cells["idJuego"].Value.ToString();
            _JuegoManejador.EliminarJuego(idJuego);

        }
        private void CrearPdf()
        {
            Document documento = new Document(PageSize.A4);
            PdfWriter.GetInstance(documento, new FileStream("C:\\PDF\\reporteJuego.pdf", FileMode.OpenOrCreate));

            documento.Open();
            iTextSharp.text.Image logo = iTextSharp.text.Image.GetInstance(@"C:\Users\churr\source\repos\ClubAjedrez\ClubAjedrezz\Resources\icons8-chessboard-96.png");
            logo.SetAbsolutePosition(30, 700);
            logo.ScaleAbsolute(60, 60);
            documento.Add(logo);

            Paragraph titulo = new Paragraph();
            titulo.Alignment = Element.ALIGN_CENTER;
            titulo.Font = FontFactory.GetFont("Arial", 20);
            titulo.Add("Juego");
            documento.Add(titulo);

            documento.Add(new Paragraph(" "));
            Paragraph subtitulo = new Paragraph();
            subtitulo.Alignment = 1;
            subtitulo.Add("Reporte de lista de juegos");
            documento.Add(subtitulo);
            documento.Add(new Paragraph(" "));

            PdfPTable tabla = new PdfPTable(4);

            PdfPCell tituloTabla = new PdfPCell(new Phrase("Datos Juegos"));
            float[] medidas = { 2.55f, 2.55f, 2.55f, 2.55f };
            tabla.SetWidths(medidas);
            tituloTabla.Colspan = 4;
            tituloTabla.HorizontalAlignment = 1;
            tabla.AddCell(tituloTabla);

            documento.Add(new Paragraph(" "));

            PdfPCell subTituloidJuego = new PdfPCell(new Phrase("idJuego"));
            subTituloidJuego.HorizontalAlignment = 1;
            tabla.AddCell(subTituloidJuego);


            PdfPCell subTituloColor = new PdfPCell(new Phrase("Color"));
            subTituloColor.HorizontalAlignment = 1;
            tabla.AddCell(subTituloColor);


            PdfPCell subTituloIdJugador = new PdfPCell(new Phrase("Jugador_idJugador"));
            subTituloIdJugador.HorizontalAlignment = 1;
            tabla.AddCell(subTituloIdJugador);


            PdfPCell subTituloIdPartida = new PdfPCell(new Phrase("Partida_idPartida"));
            subTituloIdPartida.HorizontalAlignment = 1;
            tabla.AddCell(subTituloIdPartida);

            var listaJuego = _JuegoManejador.ObtenerJuego("");
            foreach (var juego in listaJuego)
            {
                tabla.AddCell(new Phrase(juego.IdJuego));
                tabla.AddCell(new Phrase(juego.Color1));
                tabla.AddCell(new Phrase(juego.Jugador_idJugador1));
                tabla.AddCell(new Phrase(juego.Partida_idPartida1));
            }

            documento.Add(tabla);
            documento.Close();

            System.Diagnostics.Process.Start(@"C:\\PDF\\reporteJuego.pdf");
        }
        #endregion


    }
}
