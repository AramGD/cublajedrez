﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocioClubAjedrez;
using EntidadesClubAjedrez;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.text;

namespace ClubAjedrezz
{
    public partial class FrmArbitro : Form
    {
        #region Variables
        private ArbitroManejador _ArbitroManejador;
        private Arbitro _arbitro;
        private string banderaGuardar;
        #endregion
        #region Contructor
        public FrmArbitro()
        {
            InitializeComponent();
            _ArbitroManejador = new ArbitroManejador();
            _arbitro = new Arbitro();
        }
        #endregion
        #region Eventos
        private void FrmArbitro_Load(object sender, EventArgs e)
        {
            Botonera(true, false, false, true);
            BuscarArbitro("");
            Cuadros(false, false);
            LimpiarCuadros();
        }
        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Botonera(false, true, true, false);
            Cuadros(true, true);
            txtNo.Focus();
            banderaGuardar = "guardar";
        }
        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea eliminar el arbitro actual", "Eliminar arbitro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Eliminar();
                BuscarArbitro("");
                MessageBox.Show("Arbitro eliminado");
            }
        }
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (banderaGuardar == "guardar")
            {

                GuardarArbitro();
                BuscarArbitro("");


            }
            else
            {
                Actualizar();
                BuscarArbitro("");

            }

            Botonera(true, false, false, true);
            LimpiarCuadros();
            Cuadros(false, false);
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Botonera(true, false, false, true);
            LimpiarCuadros();
            Cuadros(false, false);
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarArbitro(txtBuscar.Text);
        }
        private void dtvArbitro_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Botonera(false, true, true, false);
            Cuadros(false, true);
            txtNo.Focus();
            txtNo.Text = dtvArbitro.CurrentRow.Cells["No1"].Value.ToString();
            txtIdArbitro.Text = dtvArbitro.CurrentRow.Cells["idArbitro"].Value.ToString();
            banderaGuardar = "actualizar";
        }
        private void btnPdf_Click(object sender, EventArgs e)
        {
            CrearPdf();
        }
        #endregion
        #region Metodos
        private void Botonera(Boolean nuevo, Boolean guardar, Boolean cancelar, Boolean eliminar)
        {
            btnNuevo.Enabled = nuevo;
            btnGuardar.Enabled = guardar;
            btnCancelar.Enabled = cancelar;
            btnEliminar.Enabled = eliminar;

        }
        private void BuscarArbitro(string consulta)
        {
            dtvArbitro.DataSource = _ArbitroManejador.ObtenerArbitro(consulta);

        }
        private void Cuadros(Boolean No, Boolean idArbitro)
        {
            txtNo.Enabled = No;
            txtIdArbitro.Enabled = idArbitro;
        }
        private void LimpiarCuadros()
        {

            txtNo.Text = "";
            txtIdArbitro.Text = "";
        }
        private void GuardarArbitro()
        {
            Arbitro arbitro = new Arbitro();
            arbitro.No1 = txtNo.Text;
            arbitro.IdArbitro = txtIdArbitro.Text;


            var valida = _ArbitroManejador.ValidarArbitro(arbitro);

            if (valida.Item1)
            {
                _ArbitroManejador.GuardarArbitro(arbitro);
                MessageBox.Show("El arbitro se guardo correctamente");

            }
            else
            {

                MessageBox.Show(valida.Item2, "Ocurrio un error al guardar el arbitro", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }
        private void Actualizar()
        {

            _ArbitroManejador.ActualizarArbitro(new Arbitro
            {

                No1 = txtNo.Text,
                IdArbitro = txtIdArbitro.Text

            });

        }
        private void Eliminar()
        {
            var idArbitro = dtvArbitro.CurrentRow.Cells["idArbitro"].Value.ToString();
            _ArbitroManejador.EliminarArbitro(idArbitro);

        }
        private void CrearPdf()
        {
            Document documento = new Document(PageSize.A4);
            PdfWriter.GetInstance(documento, new FileStream("C:\\PDF\\reporteArbitro.pdf", FileMode.OpenOrCreate));

            documento.Open();
            iTextSharp.text.Image logo = iTextSharp.text.Image.GetInstance(@"C:\Users\churr\source\repos\ClubAjedrez\ClubAjedrezz\Resources\icons8-chessboard-96.png");
            logo.SetAbsolutePosition(30, 700);
            logo.ScaleAbsolute(60, 60);
            documento.Add(logo);

            Paragraph titulo = new Paragraph();
            titulo.Alignment = Element.ALIGN_CENTER;
            titulo.Font = FontFactory.GetFont("Arial", 20);
            titulo.Add("Arbitro");
            documento.Add(titulo);

            documento.Add(new Paragraph(" "));
            Paragraph subtitulo = new Paragraph();
            subtitulo.Alignment = 1;
            subtitulo.Add("Reporte de lista de arbitros");
            documento.Add(subtitulo);
            documento.Add(new Paragraph(" "));

            PdfPTable tabla = new PdfPTable(2);

            PdfPCell tituloTabla = new PdfPCell(new Phrase("Datos Arbitro"));
            float[] medidas = { 2.55f, 2.55f };
            tabla.SetWidths(medidas);
            tituloTabla.Colspan = 2;
            tituloTabla.HorizontalAlignment = 1;
            tabla.AddCell(tituloTabla);

            PdfPCell subTituloNo = new PdfPCell(new Phrase("No."));
            subTituloNo.HorizontalAlignment = 1;
            tabla.AddCell(subTituloNo);

            PdfPCell subTituloIdArbitro = new PdfPCell(new Phrase("IdArbitro"));
            subTituloIdArbitro.HorizontalAlignment = 1;
            tabla.AddCell(subTituloIdArbitro);

            var listaArbitro = _ArbitroManejador.ObtenerArbitro("");
            foreach (var arbitro in listaArbitro)
            {
                tabla.AddCell(new Phrase(arbitro.No1));
                tabla.AddCell(new Phrase(arbitro.IdArbitro));
            }

            documento.Add(tabla);
            documento.Close();

            System.Diagnostics.Process.Start(@"C:\\PDF\\reporteArbitro.pdf");
        }
        #endregion
    }
}
