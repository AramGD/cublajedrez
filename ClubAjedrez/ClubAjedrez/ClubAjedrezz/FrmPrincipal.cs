﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClubAjedrezz
{
    public partial class FrmPrincipal : Form
    {
        public FrmPrincipal()
        {
            InitializeComponent();
        }
        #region EventoTablas
        private void hotelToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FrmHotel frmHotel = new FrmHotel();

            frmHotel.ShowDialog();
        }
        private void salaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmSala frmSala = new FrmSala();

            frmSala.ShowDialog();
        }
        private void arbitroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmArbitro frmArbitro = new FrmArbitro();

            frmArbitro.ShowDialog();
        }
        private void partidaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmPartida frmPartida = new FrmPartida();

            frmPartida.ShowDialog();
        }
        private void movimientoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmMovimiento frmMovimiento = new FrmMovimiento();

            frmMovimiento.ShowDialog();
        }
        private void jugadorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmJugador frmJugador = new FrmJugador();

            frmJugador.ShowDialog();
        }
        private void juegoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmJuego frmJuego = new FrmJuego();

            frmJuego.ShowDialog();
        }
        private void paisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmPais frmPais = new FrmPais();

            frmPais.ShowDialog();
        }
        private void participanteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmParticipante frmParticipante = new FrmParticipante();

            frmParticipante.ShowDialog();
        }
        private void reservaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmReserva frmReserva = new FrmReserva();

            frmReserva.ShowDialog();
        }
        private void campeonatoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmCampeonato frmCampeonato = new FrmCampeonato();

            frmCampeonato.ShowDialog();
        }
        private void msPrincipal_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
        #endregion
        #region Salida
        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        #endregion
        #region CargarDatos
        private void FrmPrincipal_FormClosing(object sender, FormClosingEventArgs e)
        {
            switch (e.CloseReason)
            {
                case CloseReason.UserClosing:
                    e.Cancel = true;
                    break;
            }
        }


        #endregion
    }
}
