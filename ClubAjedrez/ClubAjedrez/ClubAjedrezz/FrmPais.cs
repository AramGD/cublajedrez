﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocioClubAjedrez;
using EntidadesClubAjedrez;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.text;

namespace ClubAjedrezz
{
    public partial class FrmPais : Form
    {
        #region Variables
        private PaisManejador _PaisManejador;
        private Pais _pais;
        private string banderaGuardar;
        #endregion
        #region Constructor
        public FrmPais()
        {
            InitializeComponent();
            _PaisManejador = new PaisManejador();
            _pais = new Pais();
        }
        #endregion
        #region Eventos
        private void FrmPais_Load(object sender, EventArgs e)
        {
            Botonera(true, false, false, true);
            BuscarPais("");
            Cuadros(false, false, false, false);
            LimpiarCuadros();
        }
        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Botonera(false, true, true, false);
            Cuadros(true, true, true, true);
            txtIdPais.Focus();
            banderaGuardar = "guardar";
        }
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (banderaGuardar == "guardar")
            {

                GuardarPais();
                BuscarPais("");


            }
            else
            {
                Actualizar();
                BuscarPais("");

            }

            Botonera(true, false, false, true);
            LimpiarCuadros();
            Cuadros(false, false, false, false);
        }
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Botonera(true, false, false, true);
            LimpiarCuadros();
            Cuadros(false, false, false, false);
        }
        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarPais(txtBuscar.Text);
        }
        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea eliminar el pais actual", "Eliminar pais", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Eliminar();
                BuscarPais("");
                MessageBox.Show("Pais eliminada");
            }
        }
        private void dtvPais_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Botonera(false, true, true, false);
            Cuadros(false, true, true, true);
            txtIdPais.Focus();
            txtIdPais.Text = dtvPais.CurrentRow.Cells["IdPais"].Value.ToString();
            txtNombre.Text = dtvPais.CurrentRow.Cells["Nombre"].Value.ToString();
            txtNumeroClubs.Text = dtvPais.CurrentRow.Cells["NumeroClubs"].Value.ToString();
            txtRepresenta.Text = dtvPais.CurrentRow.Cells["Representa"].Value.ToString();
            banderaGuardar = "actualizar";
        }
        private void btnPdf_Click(object sender, EventArgs e)
        {
            CrearPdf();
        }

        #endregion
        #region Metodos
        private void Botonera(Boolean nuevo, Boolean guardar, Boolean cancelar, Boolean eliminar)
        {
            btnNuevo.Enabled = nuevo;
            btnGuardar.Enabled = guardar;
            btnCancelar.Enabled = cancelar;
            btnEliminar.Enabled = eliminar;

        }
        private void BuscarPais(string consulta)
        {
            dtvPais.DataSource = _PaisManejador.ObtenerPais(consulta);

        }
        private void Cuadros(Boolean idPais, Boolean nombre, Boolean numeroClubs, Boolean representa)
        {
            txtIdPais.Enabled = idPais;
            txtNombre.Enabled = nombre;
            txtNumeroClubs.Enabled = numeroClubs;
            txtRepresenta.Enabled = representa;
        }
        private void LimpiarCuadros()
        {

            txtIdPais.Text = "";
            txtNombre.Text = "";
            txtNumeroClubs.Text = "";
            txtRepresenta.Text = "";
        }
        private void GuardarPais()
        {
            Pais pais = new Pais();
            pais.IdPais = txtIdPais.Text;
            pais.Nombre = txtNombre.Text;
            pais.NumeroClubs = txtNumeroClubs.Text;
            pais.Representa = txtRepresenta.Text;

            var valida = _PaisManejador.ValidarPais(pais);

            if (valida.Item1)
            {
                _PaisManejador.GuardarPais(pais);
                MessageBox.Show("El pais se guardo correctamente");

            }
            else
            {

                MessageBox.Show(valida.Item2, "Ocurrio un error al guardar el pais", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }
        private void Actualizar()
        {

            _PaisManejador.ActualizarPais(new Pais
            {

                IdPais = txtIdPais.Text,
                Nombre = txtNombre.Text,
                NumeroClubs = txtNumeroClubs.Text,
                Representa = txtRepresenta.Text

            });

        }
        private void Eliminar()
        {
            var idPais = dtvPais.CurrentRow.Cells["idPais"].Value.ToString();
            _PaisManejador.EliminarPais(idPais);

        }
        private void CrearPdf()
        {
            Document documento = new Document(PageSize.A4);
            PdfWriter.GetInstance(documento, new FileStream("C:\\PDF\\reportePais.pdf", FileMode.OpenOrCreate));

            documento.Open();
            iTextSharp.text.Image logo = iTextSharp.text.Image.GetInstance(@"C:\Users\churr\source\repos\ClubAjedrez\ClubAjedrezz\Resources\icons8-chessboard-96.png");
            logo.SetAbsolutePosition(30, 700);
            logo.ScaleAbsolute(60, 60);
            documento.Add(logo);

            Paragraph titulo = new Paragraph();
            titulo.Alignment = Element.ALIGN_CENTER;
            titulo.Font = FontFactory.GetFont("Arial", 20);
            titulo.Add("Pais");
            documento.Add(titulo);

            documento.Add(new Paragraph(" "));
            Paragraph subtitulo = new Paragraph();
            subtitulo.Alignment = 1;
            subtitulo.Add("Reporte de lista de Paises");
            documento.Add(subtitulo);
            documento.Add(new Paragraph(" "));

            PdfPTable tabla = new PdfPTable(4);

            PdfPCell tituloTabla = new PdfPCell(new Phrase("Datos Paises"));
            float[] medidas = { 2.55f, 2.55f, 2.55f, 2.55f };
            tabla.SetWidths(medidas);
            tituloTabla.Colspan = 4;
            tituloTabla.HorizontalAlignment = 1;
            tabla.AddCell(tituloTabla);

            documento.Add(new Paragraph(" "));

            PdfPCell subTituloIdPais = new PdfPCell(new Phrase("idPais"));
            subTituloIdPais.HorizontalAlignment = 1;
            tabla.AddCell(subTituloIdPais);


            PdfPCell subTituloNombre = new PdfPCell(new Phrase("nombre"));
            subTituloNombre.HorizontalAlignment = 1;
            tabla.AddCell(subTituloNombre);


            PdfPCell subTituloNumeroClubs = new PdfPCell(new Phrase("numeroClubs"));
            subTituloNumeroClubs.HorizontalAlignment = 1;
            tabla.AddCell(subTituloNumeroClubs);


            PdfPCell subTituloRepresenta = new PdfPCell(new Phrase("representa"));
            subTituloRepresenta.HorizontalAlignment = 1;
            tabla.AddCell(subTituloRepresenta);

            var listaPais = _PaisManejador.ObtenerPais("");
            foreach (var pais in listaPais)
            {
                tabla.AddCell(new Phrase(pais.IdPais));
                tabla.AddCell(new Phrase(pais.Nombre));
                tabla.AddCell(new Phrase(pais.NumeroClubs));
                tabla.AddCell(new Phrase(pais.Representa));
            }

            documento.Add(tabla);
            documento.Close();

            System.Diagnostics.Process.Start(@"C:\\PDF\\reportePais.pdf");
        }
        #endregion


    }
}
