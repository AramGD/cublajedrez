﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocioClubAjedrez;
using EntidadesClubAjedrez;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.text;

namespace ClubAjedrezz
{
    public partial class FrmParticipante : Form
    {
        #region Variables
        private ParticipanteManejador _ParticipanteManejador;
        private Participante _participante;
        private string banderaGuardar;
        #endregion
        #region Contructor
        public FrmParticipante()
        {
            InitializeComponent();
            _ParticipanteManejador = new ParticipanteManejador();
            _participante = new Participante();
        }
        #endregion
        #region Eventos
        private void FrmParticipante_Load(object sender, EventArgs e)
        {
            Botonera(true, false, false, true);
            BuscarParticipante("");
            Cuadros(false, false, false, false);
            LimpiarCuadros();
        }
        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Botonera(false, true, true, false);
            Cuadros(true, true, true, true);
            txtNSocio.Focus();
            banderaGuardar = "guardar";
        }
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (banderaGuardar == "guardar")
            {

                GuardarParticipante();
                BuscarParticipante("");


            }
            else
            {
                Actualizar();
                BuscarParticipante("");

            }

            Botonera(true, false, false, true);
            LimpiarCuadros();
            Cuadros(false, false, false, false);
        }
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Botonera(true, false, false, true);
            LimpiarCuadros();
            Cuadros(false, false, false, false);
        }
        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarParticipante(txtBuscar.Text);
        }
        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea eliminar el participante actual", "Eliminar participante", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Eliminar();
                BuscarParticipante("");
                MessageBox.Show("participante eliminada");
            }
        }
        private void dtvParticipante_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Botonera(false, true, true, false);
            Cuadros(false, true, true, true);
            txtNSocio.Focus();
            txtNSocio.Text = dtvParticipante.CurrentRow.Cells["NSocio"].Value.ToString();
            txtNombre.Text = dtvParticipante.CurrentRow.Cells["Nombre"].Value.ToString();
            txtDireccion.Text = dtvParticipante.CurrentRow.Cells["Direccion"].Value.ToString();
            txtIdPais.Text = dtvParticipante.CurrentRow.Cells["Pais_idPais1"].Value.ToString();
            banderaGuardar = "actualizar";
        }
        private void btnPdf_Click(object sender, EventArgs e)
        {
            CrearPdf();
        }

        #endregion
        #region Metodos
        private void Botonera(Boolean nuevo, Boolean guardar, Boolean cancelar, Boolean eliminar)
        {
            btnNuevo.Enabled = nuevo;
            btnGuardar.Enabled = guardar;
            btnCancelar.Enabled = cancelar;
            btnEliminar.Enabled = eliminar;

        }
        private void BuscarParticipante(string consulta)
        {
            dtvParticipante.DataSource = _ParticipanteManejador.ObtenerParticipante(consulta);

        }
        private void Cuadros(Boolean nSocio, Boolean nombre, Boolean direccion, Boolean idPais)
        {
            txtNSocio.Enabled = nSocio;
            txtNombre.Enabled = nombre;
            txtDireccion.Enabled = direccion;
            txtIdPais.Enabled = idPais;
        }
        private void LimpiarCuadros()
        {

            txtNSocio.Text = "";
            txtNombre.Text = "";
            txtDireccion.Text = "";
            txtIdPais.Text = "";
        }
        private void GuardarParticipante()
        {
            Participante participante = new Participante();
            participante.NSocio = txtNSocio.Text;
            participante.Nombre = txtNombre.Text;
            participante.Direccion = txtDireccion.Text;
            participante.Pais_idPais1 = txtIdPais.Text;

            var valida = _ParticipanteManejador.ValidarParticipante(participante);

            if (valida.Item1)
            {
                _ParticipanteManejador.GuardarParticipante(participante);
                MessageBox.Show("El participante se guardo correctamente");

            }
            else
            {

                MessageBox.Show(valida.Item2, "Ocurrio un error al guardar el participante", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }
        private void Actualizar()
        {

            _ParticipanteManejador.ActualizarParticipante(new Participante
            {

                NSocio = txtNSocio.Text,
                Nombre = txtNombre.Text,
                Direccion = txtDireccion.Text,
                Pais_idPais1 = txtIdPais.Text

            });

        }
        private void Eliminar()
        {
            var idParticipante = dtvParticipante.CurrentRow.Cells["nSocio"].Value.ToString();
            _ParticipanteManejador.EliminarParticipante(idParticipante);

        }
        private void CrearPdf()
        {
            Document documento = new Document(PageSize.A4);
            PdfWriter.GetInstance(documento, new FileStream("C:\\PDF\\reporteParticipante.pdf", FileMode.OpenOrCreate));

            documento.Open();
            iTextSharp.text.Image logo = iTextSharp.text.Image.GetInstance(@"C:\Users\churr\source\repos\ClubAjedrez\ClubAjedrezz\Resources\icons8-chessboard-96.png");
            logo.SetAbsolutePosition(30, 700);
            logo.ScaleAbsolute(60, 60);
            documento.Add(logo);

            Paragraph titulo = new Paragraph();
            titulo.Alignment = Element.ALIGN_CENTER;
            titulo.Font = FontFactory.GetFont("Arial", 20);
            titulo.Add("Participante");
            documento.Add(titulo);

            documento.Add(new Paragraph(" "));
            Paragraph subtitulo = new Paragraph();
            subtitulo.Alignment = 1;
            subtitulo.Add("Reporte de lista de los participantes");
            documento.Add(subtitulo);
            documento.Add(new Paragraph(" "));

            PdfPTable tabla = new PdfPTable(4);

            PdfPCell tituloTabla = new PdfPCell(new Phrase("Datos participantes"));
            float[] medidas = { 2.55f, 2.55f, 2.55f, 2.55f };
            tabla.SetWidths(medidas);
            tituloTabla.Colspan = 4;
            tituloTabla.HorizontalAlignment = 1;
            tabla.AddCell(tituloTabla);

            documento.Add(new Paragraph(" "));

            PdfPCell subTituloNSocio = new PdfPCell(new Phrase("nSocio"));
            subTituloNSocio.HorizontalAlignment = 1;
            tabla.AddCell(subTituloNSocio);


            PdfPCell subTituloNombre = new PdfPCell(new Phrase("nombre"));
            subTituloNombre.HorizontalAlignment = 1;
            tabla.AddCell(subTituloNombre);


            PdfPCell subTituloDireccion = new PdfPCell(new Phrase("direccion"));
            subTituloDireccion.HorizontalAlignment = 1;
            tabla.AddCell(subTituloDireccion);


            PdfPCell subTituloIdPais = new PdfPCell(new Phrase("Pais_idPais"));
            subTituloIdPais.HorizontalAlignment = 1;
            tabla.AddCell(subTituloIdPais);

            var listaParticipante = _ParticipanteManejador.ObtenerParticipante("");
            foreach (var participante in listaParticipante)
            {
                tabla.AddCell(new Phrase(participante.NSocio));
                tabla.AddCell(new Phrase(participante.Nombre));
                tabla.AddCell(new Phrase(participante.Direccion));
                tabla.AddCell(new Phrase(participante.Pais_idPais1));
            }

            documento.Add(tabla);
            documento.Close();

            System.Diagnostics.Process.Start(@"C:\\PDF\\reporteParticipante.pdf");
        }
        #endregion


    }
}
