﻿namespace ClubAjedrezz
{
    partial class FrmPartida
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtIdArbitro = new System.Windows.Forms.TextBox();
            this.lblIdArbitro = new System.Windows.Forms.Label();
            this.txtEntrada = new System.Windows.Forms.TextBox();
            this.lblEntrada = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBuscar = new System.Windows.Forms.TextBox();
            this.dtvPartida = new System.Windows.Forms.DataGridView();
            this.txtJornada = new System.Windows.Forms.TextBox();
            this.txtIdPartida = new System.Windows.Forms.TextBox();
            this.lblJornada = new System.Windows.Forms.Label();
            this.lblIdPartida = new System.Windows.Forms.Label();
            this.btnPdf = new System.Windows.Forms.Button();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnNuevo = new System.Windows.Forms.Button();
            this.txtIdSala = new System.Windows.Forms.TextBox();
            this.lblIdSala = new System.Windows.Forms.Label();
            this.txtIdHotel = new System.Windows.Forms.TextBox();
            this.lblIdHotel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dtvPartida)).BeginInit();
            this.SuspendLayout();
            // 
            // txtIdArbitro
            // 
            this.txtIdArbitro.Location = new System.Drawing.Point(15, 196);
            this.txtIdArbitro.Name = "txtIdArbitro";
            this.txtIdArbitro.Size = new System.Drawing.Size(622, 20);
            this.txtIdArbitro.TabIndex = 58;
            // 
            // lblIdArbitro
            // 
            this.lblIdArbitro.AutoSize = true;
            this.lblIdArbitro.BackColor = System.Drawing.Color.Teal;
            this.lblIdArbitro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIdArbitro.ForeColor = System.Drawing.Color.White;
            this.lblIdArbitro.Location = new System.Drawing.Point(12, 177);
            this.lblIdArbitro.Name = "lblIdArbitro";
            this.lblIdArbitro.Size = new System.Drawing.Size(71, 16);
            this.lblIdArbitro.TabIndex = 57;
            this.lblIdArbitro.Text = "Id Arbitro";
            // 
            // txtEntrada
            // 
            this.txtEntrada.Location = new System.Drawing.Point(15, 154);
            this.txtEntrada.Name = "txtEntrada";
            this.txtEntrada.Size = new System.Drawing.Size(622, 20);
            this.txtEntrada.TabIndex = 56;
            // 
            // lblEntrada
            // 
            this.lblEntrada.AutoSize = true;
            this.lblEntrada.BackColor = System.Drawing.Color.Teal;
            this.lblEntrada.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEntrada.ForeColor = System.Drawing.Color.White;
            this.lblEntrada.Location = new System.Drawing.Point(12, 135);
            this.lblEntrada.Name = "lblEntrada";
            this.lblEntrada.Size = new System.Drawing.Size(62, 16);
            this.lblEntrada.TabIndex = 55;
            this.lblEntrada.Text = "Entrada";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Teal;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 16);
            this.label1.TabIndex = 54;
            this.label1.Text = "Buscar";
            // 
            // txtBuscar
            // 
            this.txtBuscar.Location = new System.Drawing.Point(15, 28);
            this.txtBuscar.Name = "txtBuscar";
            this.txtBuscar.Size = new System.Drawing.Size(622, 20);
            this.txtBuscar.TabIndex = 53;
            this.txtBuscar.TextChanged += new System.EventHandler(this.txtBuscar_TextChanged);
            // 
            // dtvPartida
            // 
            this.dtvPartida.BackgroundColor = System.Drawing.Color.DarkSlateGray;
            this.dtvPartida.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtvPartida.GridColor = System.Drawing.Color.White;
            this.dtvPartida.Location = new System.Drawing.Point(15, 320);
            this.dtvPartida.Name = "dtvPartida";
            this.dtvPartida.Size = new System.Drawing.Size(622, 126);
            this.dtvPartida.TabIndex = 47;
            this.dtvPartida.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtvPartida_CellDoubleClick);
            // 
            // txtJornada
            // 
            this.txtJornada.Location = new System.Drawing.Point(15, 112);
            this.txtJornada.Name = "txtJornada";
            this.txtJornada.Size = new System.Drawing.Size(622, 20);
            this.txtJornada.TabIndex = 46;
            // 
            // txtIdPartida
            // 
            this.txtIdPartida.Location = new System.Drawing.Point(15, 70);
            this.txtIdPartida.Name = "txtIdPartida";
            this.txtIdPartida.Size = new System.Drawing.Size(622, 20);
            this.txtIdPartida.TabIndex = 45;
            // 
            // lblJornada
            // 
            this.lblJornada.AutoSize = true;
            this.lblJornada.BackColor = System.Drawing.Color.Teal;
            this.lblJornada.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblJornada.ForeColor = System.Drawing.Color.White;
            this.lblJornada.Location = new System.Drawing.Point(12, 93);
            this.lblJornada.Name = "lblJornada";
            this.lblJornada.Size = new System.Drawing.Size(65, 16);
            this.lblJornada.TabIndex = 44;
            this.lblJornada.Text = "Jornada";
            // 
            // lblIdPartida
            // 
            this.lblIdPartida.AutoSize = true;
            this.lblIdPartida.BackColor = System.Drawing.Color.Teal;
            this.lblIdPartida.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIdPartida.ForeColor = System.Drawing.Color.White;
            this.lblIdPartida.Location = new System.Drawing.Point(12, 51);
            this.lblIdPartida.Name = "lblIdPartida";
            this.lblIdPartida.Size = new System.Drawing.Size(75, 16);
            this.lblIdPartida.TabIndex = 43;
            this.lblIdPartida.Text = "Id Partida";
            // 
            // btnPdf
            // 
            this.btnPdf.BackColor = System.Drawing.Color.Teal;
            this.btnPdf.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPdf.ForeColor = System.Drawing.Color.White;
            this.btnPdf.Image = global::ClubAjedrezz.Properties.Resources.Icons8_pdf_64;
            this.btnPdf.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPdf.Location = new System.Drawing.Point(15, 452);
            this.btnPdf.Name = "btnPdf";
            this.btnPdf.Size = new System.Drawing.Size(96, 46);
            this.btnPdf.TabIndex = 48;
            this.btnPdf.Text = "PDF";
            this.btnPdf.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPdf.UseVisualStyleBackColor = false;
            this.btnPdf.Click += new System.EventHandler(this.btnPdf_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.BackColor = System.Drawing.Color.Teal;
            this.btnEliminar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminar.ForeColor = System.Drawing.Color.White;
            this.btnEliminar.Image = global::ClubAjedrezz.Properties.Resources.icons8_delete_property;
            this.btnEliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEliminar.Location = new System.Drawing.Point(516, 452);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(121, 44);
            this.btnEliminar.TabIndex = 52;
            this.btnEliminar.Text = "  Eliminar";
            this.btnEliminar.UseVisualStyleBackColor = false;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.BackColor = System.Drawing.Color.Teal;
            this.btnCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.ForeColor = System.Drawing.Color.White;
            this.btnCancelar.Image = global::ClubAjedrezz.Properties.Resources.icons8_cancel_2;
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelar.Location = new System.Drawing.Point(379, 452);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(131, 44);
            this.btnCancelar.TabIndex = 51;
            this.btnCancelar.Text = "  Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = false;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.BackColor = System.Drawing.Color.Teal;
            this.btnGuardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardar.ForeColor = System.Drawing.Color.White;
            this.btnGuardar.Image = global::ClubAjedrezz.Properties.Resources.icons8_save_close;
            this.btnGuardar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGuardar.Location = new System.Drawing.Point(241, 451);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(132, 47);
            this.btnGuardar.TabIndex = 50;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = false;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnNuevo
            // 
            this.btnNuevo.BackColor = System.Drawing.Color.Teal;
            this.btnNuevo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNuevo.ForeColor = System.Drawing.Color.White;
            this.btnNuevo.Image = global::ClubAjedrezz.Properties.Resources.icons8_add_property;
            this.btnNuevo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNuevo.Location = new System.Drawing.Point(117, 451);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(118, 47);
            this.btnNuevo.TabIndex = 49;
            this.btnNuevo.Text = "Nuevo";
            this.btnNuevo.UseVisualStyleBackColor = false;
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // txtIdSala
            // 
            this.txtIdSala.Location = new System.Drawing.Point(15, 238);
            this.txtIdSala.Name = "txtIdSala";
            this.txtIdSala.Size = new System.Drawing.Size(622, 20);
            this.txtIdSala.TabIndex = 60;
            // 
            // lblIdSala
            // 
            this.lblIdSala.AutoSize = true;
            this.lblIdSala.BackColor = System.Drawing.Color.Teal;
            this.lblIdSala.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIdSala.ForeColor = System.Drawing.Color.White;
            this.lblIdSala.Location = new System.Drawing.Point(12, 219);
            this.lblIdSala.Name = "lblIdSala";
            this.lblIdSala.Size = new System.Drawing.Size(57, 16);
            this.lblIdSala.TabIndex = 59;
            this.lblIdSala.Text = "Id Sala";
            // 
            // txtIdHotel
            // 
            this.txtIdHotel.Location = new System.Drawing.Point(15, 280);
            this.txtIdHotel.Name = "txtIdHotel";
            this.txtIdHotel.Size = new System.Drawing.Size(622, 20);
            this.txtIdHotel.TabIndex = 62;
            // 
            // lblIdHotel
            // 
            this.lblIdHotel.AutoSize = true;
            this.lblIdHotel.BackColor = System.Drawing.Color.Teal;
            this.lblIdHotel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIdHotel.ForeColor = System.Drawing.Color.White;
            this.lblIdHotel.Location = new System.Drawing.Point(12, 261);
            this.lblIdHotel.Name = "lblIdHotel";
            this.lblIdHotel.Size = new System.Drawing.Size(62, 16);
            this.lblIdHotel.TabIndex = 61;
            this.lblIdHotel.Text = "Id Hotel";
            // 
            // FrmPartida
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(651, 510);
            this.Controls.Add(this.txtIdHotel);
            this.Controls.Add(this.lblIdHotel);
            this.Controls.Add(this.txtIdSala);
            this.Controls.Add(this.lblIdSala);
            this.Controls.Add(this.txtIdArbitro);
            this.Controls.Add(this.lblIdArbitro);
            this.Controls.Add(this.txtEntrada);
            this.Controls.Add(this.lblEntrada);
            this.Controls.Add(this.btnPdf);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtBuscar);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.btnNuevo);
            this.Controls.Add(this.dtvPartida);
            this.Controls.Add(this.txtJornada);
            this.Controls.Add(this.txtIdPartida);
            this.Controls.Add(this.lblJornada);
            this.Controls.Add(this.lblIdPartida);
            this.Name = "FrmPartida";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmPartida";
            this.Load += new System.EventHandler(this.FrmPartida_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtvPartida)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtIdArbitro;
        private System.Windows.Forms.Label lblIdArbitro;
        private System.Windows.Forms.TextBox txtEntrada;
        private System.Windows.Forms.Label lblEntrada;
        private System.Windows.Forms.Button btnPdf;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBuscar;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Button btnNuevo;
        private System.Windows.Forms.DataGridView dtvPartida;
        private System.Windows.Forms.TextBox txtJornada;
        private System.Windows.Forms.TextBox txtIdPartida;
        private System.Windows.Forms.Label lblJornada;
        private System.Windows.Forms.Label lblIdPartida;
        private System.Windows.Forms.TextBox txtIdSala;
        private System.Windows.Forms.Label lblIdSala;
        private System.Windows.Forms.TextBox txtIdHotel;
        private System.Windows.Forms.Label lblIdHotel;
    }
}