﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocioClubAjedrez;
using EntidadesClubAjedrez;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.text;

namespace ClubAjedrezz
{
    public partial class FrmCampeonato : Form
    {
        #region Variables
        private CampeonatoManejador _CampeonatoManejador;
        private Campeonato _campeonato;
        private string banderaGuardar;
        #endregion
        #region Constructor
        public FrmCampeonato()
        {
            InitializeComponent();
            _CampeonatoManejador = new CampeonatoManejador();
            _campeonato = new Campeonato();
        }
        #endregion
        #region Eventos
        private void FrmCampeonato_Load(object sender, EventArgs e)
        {
            Botonera(true, false, false, true);
            BuscarCampeonato("");
            Cuadros(false, false, false, false);
            LimpiarCuadros();
        }
        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Botonera(false, true, true, false);
            Cuadros(true, true, true, true);
            txtIdCampeonato.Focus();
            banderaGuardar = "guardar";
        }
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (banderaGuardar == "guardar")
            {

                GuardarCampeonato();
                BuscarCampeonato("");


            }
            else
            {
                Actualizar();
                BuscarCampeonato("");

            }

            Botonera(true, false, false, true);
            LimpiarCuadros();
            Cuadros(false, false, false, false);
        }
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Botonera(true, false, false, true);
            LimpiarCuadros();
            Cuadros(false, false, false, false);
        }
        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarCampeonato(txtBuscar.Text);
        }
        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea eliminar el campeonato actual", "Eliminar campeonato", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Eliminar();
                BuscarCampeonato("");
                MessageBox.Show("campeonato eliminado");
            }
        }
        private void dtvReserva_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Botonera(false, true, true, false);
            Cuadros(false, true, true, true);
            txtIdCampeonato.Focus();
            txtIdCampeonato.Text = dtvCampeonato.CurrentRow.Cells["IdCampeonato"].Value.ToString();
            txtNombre.Text = dtvCampeonato.CurrentRow.Cells["Nombre"].Value.ToString();
            txtTipo.Text = dtvCampeonato.CurrentRow.Cells["Tipo"].Value.ToString();
            txtNSocio.Text = dtvCampeonato.CurrentRow.Cells["Participante_nSocio1"].Value.ToString();
            banderaGuardar = "actualizar";
        }
        private void btnPdf_Click(object sender, EventArgs e)
        {
            CrearPdf();
        }

        #endregion
        #region Metodos
        private void Botonera(Boolean nuevo, Boolean guardar, Boolean cancelar, Boolean eliminar)
        {
            btnNuevo.Enabled = nuevo;
            btnGuardar.Enabled = guardar;
            btnCancelar.Enabled = cancelar;
            btnEliminar.Enabled = eliminar;

        }
        private void BuscarCampeonato(string consulta)
        {
            dtvCampeonato.DataSource = _CampeonatoManejador.ObtenerCampeonato(consulta);

        }
        private void Cuadros(Boolean idCampeonato, Boolean nombre, Boolean tipo, Boolean nSocio)
        {
            txtIdCampeonato.Enabled = idCampeonato;
            txtNombre.Enabled = nombre;
            txtTipo.Enabled = tipo;
            txtNSocio.Enabled = nSocio;
        }
        private void LimpiarCuadros()
        {

            txtIdCampeonato.Text = "";
            txtNombre.Text = "";
            txtTipo.Text = "";
            txtNSocio.Text = "";
        }
        private void GuardarCampeonato()
        {
            Campeonato campeonato = new Campeonato();
            campeonato.IdCampeonato = txtIdCampeonato.Text;
            campeonato.Nombre = txtNombre.Text;
            campeonato.Tipo = txtTipo.Text;
            campeonato.Participante_nSocio1 = txtNSocio.Text;

            var valida = _CampeonatoManejador.ValidarCampeonato(campeonato);

            if (valida.Item1)
            {
                _CampeonatoManejador.GuardarCampeonato(campeonato);
                MessageBox.Show("El campeonato se guardo correctamente");

            }
            else
            {

                MessageBox.Show(valida.Item2, "Ocurrio un error al guardar el campeonato", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }
        private void Actualizar()
        {

            _CampeonatoManejador.ActualizarCampeonato(new Campeonato
            {

                IdCampeonato = txtIdCampeonato.Text,
                Nombre = txtNombre.Text,
                Tipo = txtTipo.Text,
                Participante_nSocio1 = txtNSocio.Text

            });

        }
        private void Eliminar()
        {
            var idCampeonato = dtvCampeonato.CurrentRow.Cells["idCampeonato"].Value.ToString();
            _CampeonatoManejador.EliminarCampeonato(idCampeonato);

        }
        private void CrearPdf()
        {
            Document documento = new Document(PageSize.A4);
            PdfWriter.GetInstance(documento, new FileStream("C:\\PDF\\reporteCampeonato.pdf", FileMode.OpenOrCreate));

            documento.Open();
            iTextSharp.text.Image logo = iTextSharp.text.Image.GetInstance(@"C:\Users\churr\source\repos\ClubAjedrez\ClubAjedrezz\Resources\icons8-chessboard-96.png");
            logo.SetAbsolutePosition(30, 700);
            logo.ScaleAbsolute(60, 60);
            documento.Add(logo);

            Paragraph titulo = new Paragraph();
            titulo.Alignment = Element.ALIGN_CENTER;
            titulo.Font = FontFactory.GetFont("Arial", 20);
            titulo.Add("Campeonato");
            documento.Add(titulo);

            documento.Add(new Paragraph(" "));
            Paragraph subtitulo = new Paragraph();
            subtitulo.Alignment = 1;
            subtitulo.Add("Reporte de lista de campeonatos");
            documento.Add(subtitulo);
            documento.Add(new Paragraph(" "));

            PdfPTable tabla = new PdfPTable(4);

            PdfPCell tituloTabla = new PdfPCell(new Phrase("Datos Campeonatos"));
            float[] medidas = { 2.55f, 2.55f, 2.55f, 2.55f };
            tabla.SetWidths(medidas);
            tituloTabla.Colspan = 4;
            tituloTabla.HorizontalAlignment = 1;
            tabla.AddCell(tituloTabla);

            documento.Add(new Paragraph(" "));

            PdfPCell subTituloidCampeonato = new PdfPCell(new Phrase("idCampeonato"));
            subTituloidCampeonato.HorizontalAlignment = 1;
            tabla.AddCell(subTituloidCampeonato);


            PdfPCell subTituloNombre = new PdfPCell(new Phrase("nombre"));
            subTituloNombre.HorizontalAlignment = 1;
            tabla.AddCell(subTituloNombre);


            PdfPCell subTituloTipo = new PdfPCell(new Phrase("tipo"));
            subTituloTipo.HorizontalAlignment = 1;
            tabla.AddCell(subTituloTipo);


            PdfPCell subTituloNSocio = new PdfPCell(new Phrase("Participante_nSocio"));
            subTituloNSocio.HorizontalAlignment = 1;
            tabla.AddCell(subTituloNSocio);

            var listaCampeonato = _CampeonatoManejador.ObtenerCampeonato("");
            foreach (var campeonato in listaCampeonato)
            {
                tabla.AddCell(new Phrase(campeonato.IdCampeonato));
                tabla.AddCell(new Phrase(campeonato.Nombre));
                tabla.AddCell(new Phrase(campeonato.Tipo));
                tabla.AddCell(new Phrase(campeonato.Participante_nSocio1));
            }

            documento.Add(tabla);
            documento.Close();

            System.Diagnostics.Process.Start(@"C:\\PDF\\reporteCampeonato.pdf");
        }
        #endregion


    }
}
