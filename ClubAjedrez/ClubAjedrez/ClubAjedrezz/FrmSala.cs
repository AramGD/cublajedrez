﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocioClubAjedrez;
using EntidadesClubAjedrez;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.text;

namespace ClubAjedrezz
{
    public partial class FrmSala : Form
    {
        #region Variables
        private SalaManejador _SalaManejador;
        private Sala _sala;
        private string banderaGuardar;
        #endregion
        #region Contructor
        public FrmSala()
        {
            InitializeComponent();
            _SalaManejador = new SalaManejador();
            _sala = new Sala();
        }
        #endregion
        #region Eventos
        private void FrmSala_Load(object sender, EventArgs e)
        {
            Botonera(true, false, false, true);
            BuscarSala("");
            Cuadros(false, false, false, false);
            LimpiarCuadros();
        }
        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Botonera(false, true, true, false);
            Cuadros(true, true, true, true);
            txtIdSala.Focus();
            banderaGuardar = "guardar";
        }
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (banderaGuardar == "guardar")
            {

                GuardarSala();
                BuscarSala("");


            }
            else
            {
                Actualizar();
                BuscarSala("");

            }

            Botonera(true, false, false, true);
            LimpiarCuadros();
            Cuadros(false, false, false, false);
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Botonera(true, false, false, true);
            LimpiarCuadros();
            Cuadros(false, false, false, false);
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarSala(txtBuscar.Text);
        }
        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea eliminar la sala actual", "Eliminar sala", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Eliminar();
                BuscarSala("");
                MessageBox.Show("Sala eliminada");
            }
        }

        private void dtvSala_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Botonera(false, true, true, false);
            Cuadros(false, true, true, true);
            txtIdSala.Focus();
            txtIdSala.Text = dtvSala.CurrentRow.Cells["idSala"].Value.ToString();
            txtCapacidad.Text = dtvSala.CurrentRow.Cells["capacidad"].Value.ToString();
            txtMedios.Text = dtvSala.CurrentRow.Cells["medios"].Value.ToString();
            txtIdHotel.Text = dtvSala.CurrentRow.Cells["Hotel_idHotel"].Value.ToString();
            banderaGuardar = "actualizar";
        }
        private void btnPdf_Click(object sender, EventArgs e)
        {
            CrearPdf();
        }
        #endregion
        #region Metodos
        private void Botonera(Boolean nuevo, Boolean guardar, Boolean cancelar, Boolean eliminar)
        {
            btnNuevo.Enabled = nuevo;
            btnGuardar.Enabled = guardar;
            btnCancelar.Enabled = cancelar;
            btnEliminar.Enabled = eliminar;

        }
        private void BuscarSala(string consulta)
        {
            dtvSala.DataSource = _SalaManejador.ObtenerSala(consulta);

        }
        private void Cuadros(Boolean idSala, Boolean capacidad, Boolean medios, Boolean idHotel)
        {
            txtIdSala.Enabled = idSala;
            txtCapacidad.Enabled = capacidad;
            txtMedios.Enabled = medios;
            txtIdHotel.Enabled = idHotel;
        }
        private void LimpiarCuadros()
        {

            txtIdSala.Text = "";
            txtCapacidad.Text = "";
            txtMedios.Text = "";
            txtIdHotel.Text = "";
        }
        private void GuardarSala()
        {
            Sala sala = new Sala();
            sala.IdSala = txtIdSala.Text;
            sala.Capacidad = txtCapacidad.Text;
            sala.Medios = txtMedios.Text;
            sala.Hotel_idHotel = txtIdHotel.Text;

            var valida = _SalaManejador.ValidarSala(sala);

            if (valida.Item1)
            {
                _SalaManejador.GuardarSala(sala);
                MessageBox.Show("La sala se guardo correctamente");

            }
            else
            {

                MessageBox.Show(valida.Item2, "Ocurrio un error al guardar la sala", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }
        private void Actualizar()
        {

            _SalaManejador.ActualizarSala(new Sala
            {

                IdSala = txtIdSala.Text,
                Capacidad = txtCapacidad.Text,
                Medios = txtMedios.Text,
                Hotel_idHotel = txtIdHotel.Text

            });

        }
        private void Eliminar()
        {
            var idSala = dtvSala.CurrentRow.Cells["idSala"].Value.ToString();
            _SalaManejador.EliminarSala(idSala);

        }
        private void CrearPdf()
        {
            Document documento = new Document(PageSize.A4);
            PdfWriter.GetInstance(documento, new FileStream("C:\\PDF\\reporteSala.pdf", FileMode.OpenOrCreate));

            documento.Open();
            iTextSharp.text.Image logo = iTextSharp.text.Image.GetInstance(@"C:\Users\churr\source\repos\ClubAjedrez\ClubAjedrezz\Resources\icons8-chessboard-96.png");
            logo.SetAbsolutePosition(30, 700);
            logo.ScaleAbsolute(60, 60);
            documento.Add(logo);

            Paragraph titulo = new Paragraph();
            titulo.Alignment = Element.ALIGN_CENTER;
            titulo.Font = FontFactory.GetFont("Arial", 20);
            titulo.Add("Sala");
            documento.Add(titulo);

            documento.Add(new Paragraph(" "));
            Paragraph subtitulo = new Paragraph();
            subtitulo.Alignment = 1;
            subtitulo.Add("Reporte de lista de salas");
            documento.Add(subtitulo);
            documento.Add(new Paragraph(" "));

            PdfPTable tabla = new PdfPTable(4);

            PdfPCell tituloTabla = new PdfPCell(new Phrase("Datos Sala"));
            float[] medidas = { 2.55f, 2.55f, 2.55f, 2.55f };
            tabla.SetWidths(medidas);
            tituloTabla.Colspan = 4;
            tituloTabla.HorizontalAlignment = 1;
            tabla.AddCell(tituloTabla);

            documento.Add(new Paragraph(" "));

            PdfPCell subTituloidSala = new PdfPCell(new Phrase("idSala"));
            subTituloidSala.HorizontalAlignment = 1;
            tabla.AddCell(subTituloidSala);


            PdfPCell subTituloCapacidad = new PdfPCell(new Phrase("Capacidad"));
            subTituloCapacidad.HorizontalAlignment = 1;
            tabla.AddCell(subTituloCapacidad);


            PdfPCell subTituloMedios = new PdfPCell(new Phrase("Medios"));
            subTituloMedios.HorizontalAlignment = 1;
            tabla.AddCell(subTituloMedios);


            PdfPCell subTituloIdHotel = new PdfPCell(new Phrase("idHotel"));
            subTituloIdHotel.HorizontalAlignment = 1;
            tabla.AddCell(subTituloIdHotel);

            var listaSala = _SalaManejador.ObtenerSala("");
            foreach (var sala in listaSala)
            {
                tabla.AddCell(new Phrase(sala.IdSala));
                tabla.AddCell(new Phrase(sala.Capacidad));
                tabla.AddCell(new Phrase(sala.Medios));
                tabla.AddCell(new Phrase(sala.Hotel_idHotel));
            }

            documento.Add(tabla);
            documento.Close();

            System.Diagnostics.Process.Start(@"C:\\PDF\\reporteSala.pdf");
        }
        #endregion
    }
}
