﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocioClubAjedrez;
using EntidadesClubAjedrez;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.text;

namespace ClubAjedrezz
{
    public partial class FrmReserva : Form
    {
        #region Variables
        private ReservaManejador _ReservaManejador;
        private Reserva _reserva;
        private string banderaGuardar;
        #endregion
        #region Constructor
        public FrmReserva()
        {
            InitializeComponent();
            _ReservaManejador = new ReservaManejador();
            _reserva = new Reserva();
        }
        #endregion
        #region Eventos
        private void FrmReserva_Load(object sender, EventArgs e)
        {
            Botonera(true, false, false, true);
            BuscarReserva("");
            Cuadros(false, false, false, false, false);
            LimpiarCuadros();
        }
        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Botonera(false, true, true, false);
            Cuadros(true, true, true, true, true);
            txtIdReserva.Focus();
            banderaGuardar = "guardar";
        }
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (banderaGuardar == "guardar")
            {

                GuardarReserva();
                BuscarReserva("");


            }
            else
            {
                Actualizar();
                BuscarReserva("");

            }

            Botonera(true, false, false, true);
            LimpiarCuadros();
            Cuadros(false, false, false, false, false);
        }
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Botonera(true, false, false, true);
            LimpiarCuadros();
            Cuadros(false, false, false, false, false);
        }
        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarReserva(txtBuscar.Text);
        }
        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea eliminar la reserva actual", "Eliminar reserva", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Eliminar();
                BuscarReserva("");
                MessageBox.Show("reserva eliminada");
            }
        }
        private void dtvReserva_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Botonera(false, true, true, false);
            Cuadros(false, true, true, true, true);
            txtIdReserva.Focus();
            txtIdReserva.Text = dtvReserva.CurrentRow.Cells["IdReserva"].Value.ToString();
            txtFechaEntrada.Text = dtvReserva.CurrentRow.Cells["FechaEntrada"].Value.ToString();
            txtFechaSalida.Text = dtvReserva.CurrentRow.Cells["FechaSalida"].Value.ToString();
            txtNSocio.Text = dtvReserva.CurrentRow.Cells["Participante_nSocio1"].Value.ToString();
            txtIdHotel.Text = dtvReserva.CurrentRow.Cells["Hotel_idHotel1"].Value.ToString();
            banderaGuardar = "actualizar";
        }
        private void btnPdf_Click(object sender, EventArgs e)
        {
            CrearPdf();
        }

        #endregion
        #region Metodos
        private void Botonera(Boolean nuevo, Boolean guardar, Boolean cancelar, Boolean eliminar)
        {
            btnNuevo.Enabled = nuevo;
            btnGuardar.Enabled = guardar;
            btnCancelar.Enabled = cancelar;
            btnEliminar.Enabled = eliminar;

        }
        private void BuscarReserva(string consulta)
        {
            dtvReserva.DataSource = _ReservaManejador.ObtenerReserva(consulta);

        }
        private void Cuadros(Boolean idReserva, Boolean fechaEntrada, Boolean fechaSalida, Boolean nSocio, Boolean idHotel)
        {
            txtIdReserva.Enabled = idReserva;
            txtFechaEntrada.Enabled = fechaEntrada;
            txtFechaSalida.Enabled = fechaSalida;
            txtNSocio.Enabled = nSocio;
            txtIdHotel.Enabled = idHotel;
        }
        private void LimpiarCuadros()
        {

            txtIdReserva.Text = "";
            txtFechaEntrada.Text = "";
            txtFechaSalida.Text = "";
            txtNSocio.Text = "";
            txtIdHotel.Text = "";
        }
        private void GuardarReserva()
        {
            Reserva reserva = new Reserva();
            reserva.IdReserva = txtIdReserva.Text;
            reserva.FechaEntrada = txtFechaEntrada.Text;
            reserva.FechaSalida = txtFechaSalida.Text;
            reserva.Participante_nSocio1 = txtNSocio.Text;
            reserva.Hotel_idHotel1 = txtIdHotel.Text;

            var valida = _ReservaManejador.ValidarReserva(reserva);

            if (valida.Item1)
            {
                _ReservaManejador.GuardarReserva(reserva);
                MessageBox.Show("La reserva se guardo correctamente");

            }
            else
            {

                MessageBox.Show(valida.Item2, "Ocurrio un error al guardar la reserva", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }
        private void Actualizar()
        {

            _ReservaManejador.ActualizarReserva(new Reserva
            {

                IdReserva = txtIdReserva.Text,
                FechaEntrada = txtFechaEntrada.Text,
                FechaSalida = txtFechaSalida.Text,
                Participante_nSocio1 = txtNSocio.Text,
                Hotel_idHotel1 = txtIdHotel.Text

            });

        }
        private void Eliminar()
        {
            var idReserva = dtvReserva.CurrentRow.Cells["idReserva"].Value.ToString();
            _ReservaManejador.EliminarReserva(idReserva);

        }
        private void CrearPdf()
        {
            Document documento = new Document(PageSize.A4);
            PdfWriter.GetInstance(documento, new FileStream("C:\\PDF\\reporteReserva.pdf", FileMode.OpenOrCreate));

            documento.Open();
            iTextSharp.text.Image logo = iTextSharp.text.Image.GetInstance(@"C:\Users\churr\source\repos\ClubAjedrez\ClubAjedrezz\Resources\icons8-chessboard-96.png");
            logo.SetAbsolutePosition(30, 700);
            logo.ScaleAbsolute(60, 60);
            documento.Add(logo);

            Paragraph titulo = new Paragraph();
            titulo.Alignment = Element.ALIGN_CENTER;
            titulo.Font = FontFactory.GetFont("Arial", 20);
            titulo.Add("Reserva");
            documento.Add(titulo);

            documento.Add(new Paragraph(" "));
            Paragraph subtitulo = new Paragraph();
            subtitulo.Alignment = 1;
            subtitulo.Add("Reporte de lista de reservas");
            documento.Add(subtitulo);
            documento.Add(new Paragraph(" "));

            PdfPTable tabla = new PdfPTable(5);

            PdfPCell tituloTabla = new PdfPCell(new Phrase("Datos Reservas"));
            float[] medidas = { 2.55f, 2.55f, 2.55f, 2.55f, 2.55f };
            tabla.SetWidths(medidas);
            tituloTabla.Colspan = 5;
            tituloTabla.HorizontalAlignment = 1;
            tabla.AddCell(tituloTabla);

            documento.Add(new Paragraph(" "));

            PdfPCell subTituloidReserva = new PdfPCell(new Phrase("idReserva"));
            subTituloidReserva.HorizontalAlignment = 1;
            tabla.AddCell(subTituloidReserva);


            PdfPCell subTituloFechaEntrada = new PdfPCell(new Phrase("fechaEntrada"));
            subTituloFechaEntrada.HorizontalAlignment = 1;
            tabla.AddCell(subTituloFechaEntrada);


            PdfPCell subTituloFechaSalida = new PdfPCell(new Phrase("fechaSalida"));
            subTituloFechaSalida.HorizontalAlignment = 1;
            tabla.AddCell(subTituloFechaSalida);


            PdfPCell subTituloNSocio = new PdfPCell(new Phrase("Participante_nSocio"));
            subTituloNSocio.HorizontalAlignment = 1;
            tabla.AddCell(subTituloNSocio);

            PdfPCell subTituloIdHotel = new PdfPCell(new Phrase("Hotel_idHotel"));
            subTituloIdHotel.HorizontalAlignment = 1;
            tabla.AddCell(subTituloIdHotel);

            var listaReserva = _ReservaManejador.ObtenerReserva("");
            foreach (var reserva in listaReserva)
            {
                tabla.AddCell(new Phrase(reserva.IdReserva));
                tabla.AddCell(new Phrase(reserva.FechaEntrada));
                tabla.AddCell(new Phrase(reserva.FechaSalida));
                tabla.AddCell(new Phrase(reserva.Participante_nSocio1));
                tabla.AddCell(new Phrase(reserva.Hotel_idHotel1));
            }

            documento.Add(tabla);
            documento.Close();

            System.Diagnostics.Process.Start(@"C:\\PDF\\reporteReserva.pdf");
        }
        #endregion


    }
}
