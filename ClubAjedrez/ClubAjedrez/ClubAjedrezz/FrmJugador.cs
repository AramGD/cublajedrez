﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocioClubAjedrez;
using EntidadesClubAjedrez;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.text;

namespace ClubAjedrezz
{
    public partial class FrmJugador : Form
    {
        #region Variables
        private JugadorManejador _JugadorManejador;
        private Jugador _jugador;
        private string banderaGuardar;
        #endregion
        #region Constructor
        public FrmJugador()
        {
            InitializeComponent();
            _JugadorManejador = new JugadorManejador();
            _jugador = new Jugador();
        }
        #endregion
        #region Eventos
        private void FrmJugador_Load(object sender, EventArgs e)
        {
            Botonera(true, false, false, true);
            BuscarJugador("");
            Cuadros(false, false);
            LimpiarCuadros();
        }
        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Botonera(false, true, true, false);
            Cuadros(true, true);
            txtIdJugador.Focus();
            banderaGuardar = "guardar";
        }
        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea eliminar el jugador actual", "Eliminar jugador", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Eliminar();
                BuscarJugador("");
                MessageBox.Show("Jugador eliminado");
            }
        }
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (banderaGuardar == "guardar")
            {

                GuardarJugador();
                BuscarJugador("");


            }
            else
            {
                Actualizar();
                BuscarJugador("");

            }

            Botonera(true, false, false, true);
            LimpiarCuadros();
            Cuadros(false, false);
        }
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Botonera(true, false, false, true);
            LimpiarCuadros();
            Cuadros(false, false);
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarJugador(txtBuscar.Text);
        }
        private void dtvJugador_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Botonera(false, true, true, false);
            Cuadros(false, true);
            txtIdJugador.Focus();
            txtIdJugador.Text = dtvJugador.CurrentRow.Cells["IdJugador"].Value.ToString();
            txtNivel.Text = dtvJugador.CurrentRow.Cells["Nivel"].Value.ToString();
            banderaGuardar = "actualizar";
        }
        private void btnPdf_Click(object sender, EventArgs e)
        {
            CrearPdf();
        }
        #endregion
        #region Metodos
        private void Botonera(Boolean nuevo, Boolean guardar, Boolean cancelar, Boolean eliminar)
        {
            btnNuevo.Enabled = nuevo;
            btnGuardar.Enabled = guardar;
            btnCancelar.Enabled = cancelar;
            btnEliminar.Enabled = eliminar;

        }
        private void BuscarJugador(string consulta)
        {
            dtvJugador.DataSource = _JugadorManejador.ObtenerJugador(consulta);

        }
        private void Cuadros(Boolean idJugador, Boolean nivel)
        {
            txtIdJugador.Enabled = idJugador;
            txtNivel.Enabled = nivel;
        }
        private void LimpiarCuadros()
        {

            txtIdJugador.Text = "";
            txtNivel.Text = "";
        }
        private void GuardarJugador()
        {
            Jugador jugador = new Jugador();
            jugador.IdJugador = txtIdJugador.Text;
            jugador.Nivel = txtNivel.Text;


            var valida = _JugadorManejador.ValidarJugador(jugador);

            if (valida.Item1)
            {
                _JugadorManejador.GuardarJugador(jugador);
                MessageBox.Show("El jugador se guardo correctamente");

            }
            else
            {

                MessageBox.Show(valida.Item2, "Ocurrio un error al guardar el jugador", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }
        private void Actualizar()
        {

            _JugadorManejador.ActualizarJugador(new Jugador
            {

                IdJugador = txtIdJugador.Text,
                Nivel = txtNivel.Text

            });

        }
        private void Eliminar()
        {
            var idJugador = dtvJugador.CurrentRow.Cells["idJugador"].Value.ToString();
            _JugadorManejador.EliminarJugador(idJugador);

        }
        private void CrearPdf()
        {
            Document documento = new Document(PageSize.A4);
            PdfWriter.GetInstance(documento, new FileStream("C:\\PDF\\reporteJugador.pdf", FileMode.OpenOrCreate));

            documento.Open();
            iTextSharp.text.Image logo = iTextSharp.text.Image.GetInstance(@"C:\Users\churr\source\repos\ClubAjedrez\ClubAjedrezz\Resources\icons8-chessboard-96.png");
            logo.SetAbsolutePosition(30, 700);
            logo.ScaleAbsolute(60, 60);
            documento.Add(logo);

            Paragraph titulo = new Paragraph();
            titulo.Alignment = Element.ALIGN_CENTER;
            titulo.Font = FontFactory.GetFont("Arial", 20);
            titulo.Add("Jugador");
            documento.Add(titulo);

            documento.Add(new Paragraph(" "));
            Paragraph subtitulo = new Paragraph();
            subtitulo.Alignment = 1;
            subtitulo.Add("Reporte de lista de Jugadores");
            documento.Add(subtitulo);
            documento.Add(new Paragraph(" "));

            PdfPTable tabla = new PdfPTable(2);

            PdfPCell tituloTabla = new PdfPCell(new Phrase("Datos Jugadores"));
            float[] medidas = { 2.55f, 2.55f };
            tabla.SetWidths(medidas);
            tituloTabla.Colspan = 2;
            tituloTabla.HorizontalAlignment = 1;
            tabla.AddCell(tituloTabla);

            PdfPCell subTituloIdJugador = new PdfPCell(new Phrase("idJugador"));
            subTituloIdJugador.HorizontalAlignment = 1;
            tabla.AddCell(subTituloIdJugador);

            PdfPCell subTituloNivel = new PdfPCell(new Phrase("nivel"));
            subTituloNivel.HorizontalAlignment = 1;
            tabla.AddCell(subTituloNivel);

            var listaJugador = _JugadorManejador.ObtenerJugador("");
            foreach (var jugador in listaJugador)
            {
                tabla.AddCell(new Phrase(jugador.IdJugador));
                tabla.AddCell(new Phrase(jugador.Nivel));
            }

            documento.Add(tabla);
            documento.Close();

            System.Diagnostics.Process.Start(@"C:\\PDF\\reporteJugador.pdf");
        }
        #endregion


    }
}
