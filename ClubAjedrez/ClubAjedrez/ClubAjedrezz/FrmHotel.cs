﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocioClubAjedrez;
using EntidadesClubAjedrez;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.text;

namespace ClubAjedrezz
{
    public partial class FrmHotel : Form
    {
        #region Variables
        private HotelManejador _HotelManejador;
        private Hotel _hotel;
        private string banderaGuardar;
        #endregion
        #region Constructor
        public FrmHotel()
        {
            InitializeComponent();
            _HotelManejador = new HotelManejador();
            _hotel = new Hotel();
        }
        #endregion
        #region Eventos
        private void FrmHotel_Load(object sender, EventArgs e)
        {
            Botonera(true, false, false, true);
            BuscarHotel("");
            Cuadros(false, false, false, false);
            LimpiarCuadros();
        }
        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Botonera(false, true, true, false);
            Cuadros(true, true, true, true);
            txtIdHotel.Focus();
            banderaGuardar = "guardar";
        }
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (banderaGuardar == "guardar")
            {

                GuardarHotel();
                BuscarHotel("");


            }
            else
            {
                Actualizar();
                BuscarHotel("");

            }

            Botonera(true, false, false, true);
            LimpiarCuadros();
            Cuadros(false, false, false, false);
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Botonera(true, false, false, true);
            LimpiarCuadros();
            Cuadros(false, false, false, false);
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarHotel(txtBuscar.Text);
        }
        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea eliminar el hotel actual", "Eliminar hotel", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Eliminar();
                BuscarHotel("");
                MessageBox.Show("Hotel eliminado");
            }
        }

        private void dtvHotel_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Botonera(false, true, true, false);
            Cuadros(false, true, true, true);
            txtNombre.Focus();
            txtIdHotel.Text = dtvHotel.CurrentRow.Cells["idHotel"].Value.ToString();
            txtNombre.Text = dtvHotel.CurrentRow.Cells["nombre"].Value.ToString();
            txtDireccion.Text = dtvHotel.CurrentRow.Cells["direccion"].Value.ToString();
            txtTelefono.Text = dtvHotel.CurrentRow.Cells["telefono"].Value.ToString();
            banderaGuardar = "actualizar";
        }
        private void btnPdf_Click(object sender, EventArgs e)
        {
            CrearPdf();
        }
        #endregion
        #region Metodos
        private void Botonera(Boolean nuevo, Boolean guardar, Boolean cancelar, Boolean eliminar)
        {
            btnNuevo.Enabled = nuevo;
            btnGuardar.Enabled = guardar;
            btnCancelar.Enabled = cancelar;
            btnEliminar.Enabled = eliminar;

        }
        private void BuscarHotel(string consulta)
        {
            dtvHotel.DataSource = _HotelManejador.ObtenerHotel(consulta);

        }
        private void Cuadros(Boolean idHotel, Boolean nombre, Boolean direccion, Boolean telefono)
        {
            txtIdHotel.Enabled = idHotel;
            txtNombre.Enabled = nombre;
            txtDireccion.Enabled = direccion;
            txtTelefono.Enabled = telefono;
        }
        private void LimpiarCuadros()
        {

            txtIdHotel.Text = "";
            txtNombre.Text = "";
            txtDireccion.Text = "";
            txtTelefono.Text = "";
        }
        private void GuardarHotel()
        {
            Hotel hotel = new Hotel();
            hotel.IdHotel = txtIdHotel.Text;
            hotel.Nombre = txtNombre.Text;
            hotel.Direccion = txtDireccion.Text;
            hotel.Telefono = txtTelefono.Text;

            var valida = _HotelManejador.ValidarHotel(hotel);

            if (valida.Item1)
            {
                _HotelManejador.GuardarHotel(hotel);
                MessageBox.Show("El hotel se guardo correctamente");

            }
            else
            {

                MessageBox.Show(valida.Item2, "Ocurrio un error al guardar el hotel", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }
        private void Actualizar()
        {

            _HotelManejador.ActualizarHotel(new Hotel
            {

                IdHotel = txtIdHotel.Text,
                Nombre = txtNombre.Text,
                Direccion = txtDireccion.Text,
                Telefono = txtTelefono.Text

            });

        }
        private void Eliminar()
        {
            var idhotel = dtvHotel.CurrentRow.Cells["idHotel"].Value.ToString();
            _HotelManejador.EliminarHotel(idhotel);

        }
        private void CrearPdf()
        {
            Document documento = new Document(PageSize.A4);
            PdfWriter.GetInstance(documento, new FileStream("C:\\PDF\\reporteHotel.pdf", FileMode.OpenOrCreate));

            documento.Open();
            iTextSharp.text.Image logo = iTextSharp.text.Image.GetInstance(@"C:\Users\churr\source\repos\ClubAjedrez\ClubAjedrezz\Resources\icons8-chessboard-96.png");
            logo.SetAbsolutePosition(30, 700);
            logo.ScaleAbsolute(60, 60);
            documento.Add(logo);

            Paragraph titulo = new Paragraph();
            titulo.Alignment = Element.ALIGN_CENTER;
            titulo.Font = FontFactory.GetFont("Arial", 20);
            titulo.Add("Hotel");
            documento.Add(titulo);

            documento.Add(new Paragraph(" "));
            Paragraph subtitulo = new Paragraph();
            subtitulo.Alignment = 1;
            subtitulo.Add("Reporte de lista de hoteles");
            documento.Add(subtitulo);
            documento.Add(new Paragraph(" "));

            PdfPTable tabla = new PdfPTable(4);

            PdfPCell tituloTabla = new PdfPCell(new Phrase("Datos Hotel"));
            float[] medidas = { 2.55f, 2.55f, 2.55f, 2.55f };
            tabla.SetWidths(medidas);
            tituloTabla.Colspan = 4;
            tituloTabla.HorizontalAlignment = 1;
            tabla.AddCell(tituloTabla);

            documento.Add(new Paragraph(" "));

            PdfPCell subTituloidHotel = new PdfPCell(new Phrase("idHotel"));
            subTituloidHotel.HorizontalAlignment = 1;
            tabla.AddCell(subTituloidHotel);


            PdfPCell subTituloNombre = new PdfPCell(new Phrase("Nombre"));
            subTituloNombre.HorizontalAlignment = 1;
            tabla.AddCell(subTituloNombre);


            PdfPCell subTituloDireccion = new PdfPCell(new Phrase("Direccion"));
            subTituloDireccion.HorizontalAlignment = 1;
            tabla.AddCell(subTituloDireccion);


            PdfPCell subTituloTelefono = new PdfPCell(new Phrase("Telefono"));
            subTituloTelefono.HorizontalAlignment = 1;
            tabla.AddCell(subTituloTelefono);

            var listaHotel = _HotelManejador.ObtenerHotel("");
            foreach (var hotel in listaHotel)
            {
                tabla.AddCell(new Phrase(hotel.IdHotel));
                tabla.AddCell(new Phrase(hotel.Nombre));
                tabla.AddCell(new Phrase(hotel.Direccion));
                tabla.AddCell(new Phrase(hotel.Telefono));
            }

            documento.Add(tabla);
            documento.Close();

            System.Diagnostics.Process.Start(@"C:\\PDF\\reporteHotel.pdf");
        }
        #endregion
    }
}
