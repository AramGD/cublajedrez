﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesClubAjedrez
{
    public class Participante
    {
        private string nSocio;
        private string nombre;
        private string direccion;
        private string Pais_idPais;

        public string NSocio { get => nSocio; set => nSocio = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Direccion { get => direccion; set => direccion = value; }
        public string Pais_idPais1 { get => Pais_idPais; set => Pais_idPais = value; }
    }
}
