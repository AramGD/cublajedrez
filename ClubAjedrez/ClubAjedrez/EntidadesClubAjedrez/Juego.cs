﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesClubAjedrez
{
    public class Juego
    {
        private string idJuego;
        private string Color;
        private string Jugador_idJugador;
        private string Partida_idPartida;

        public string IdJuego { get => idJuego; set => idJuego = value; }
        public string Color1 { get => Color; set => Color = value; }
        public string Jugador_idJugador1 { get => Jugador_idJugador; set => Jugador_idJugador = value; }
        public string Partida_idPartida1 { get => Partida_idPartida; set => Partida_idPartida = value; }
    }
}
