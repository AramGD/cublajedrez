﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesClubAjedrez
{
    public class Pais
    {
        private string idPais;
        private string nombre;
        private string numeroClubs;
        private string representa;

        public string IdPais { get => idPais; set => idPais = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string NumeroClubs { get => numeroClubs; set => numeroClubs = value; }
        public string Representa { get => representa; set => representa = value; }
    }
}
