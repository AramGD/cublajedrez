﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesClubAjedrez
{
    public class Jugador
    {
        private string idJugador;
        private string nivel;

        public string IdJugador { get => idJugador; set => idJugador = value; }
        public string Nivel { get => nivel; set => nivel = value; }
    }
}
