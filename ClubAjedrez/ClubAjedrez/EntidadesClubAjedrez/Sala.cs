﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesClubAjedrez
{
   public class Sala
    {
        private string _idSala;
        private string _capacidad;
        private string _medios;
        private string _Hotel_idHotel;

        public string IdSala { get => _idSala; set => _idSala = value; }
        public string Capacidad { get => _capacidad; set => _capacidad = value; }
        public string Medios { get => _medios; set => _medios = value; }
        public string Hotel_idHotel { get => _Hotel_idHotel; set => _Hotel_idHotel = value; }
    }
}
