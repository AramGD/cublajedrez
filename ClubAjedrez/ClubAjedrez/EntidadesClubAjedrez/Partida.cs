﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesClubAjedrez
{
    public class Partida
    {
        private string idPartida;
        private string jornada;
        private string entrada;
        private string Arbitro_idArbitro;
        private string Sala_idSala;
        private string Hotel_idHotel;

        public string IdPartida { get => idPartida; set => idPartida = value; }
        public string Jornada { get => jornada; set => jornada = value; }
        public string Entrada { get => entrada; set => entrada = value; }
        public string Arbitro_idArbitro1 { get => Arbitro_idArbitro; set => Arbitro_idArbitro = value; }
        public string Sala_idSala1 { get => Sala_idSala; set => Sala_idSala = value; }
        public string Hotel_idHotel1 { get => Hotel_idHotel; set => Hotel_idHotel = value; }
    }
}
