﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesClubAjedrez
{
    public class Reserva
    {
        private string idReserva;
        private string fechaEntrada;
        private string fechaSalida;
        private string Participante_nSocio;
        private string Hotel_idHotel;

        public string IdReserva { get => idReserva; set => idReserva = value; }
        public string FechaEntrada { get => fechaEntrada; set => fechaEntrada = value; }
        public string FechaSalida { get => fechaSalida; set => fechaSalida = value; }
        public string Participante_nSocio1 { get => Participante_nSocio; set => Participante_nSocio = value; }
        public string Hotel_idHotel1 { get => Hotel_idHotel; set => Hotel_idHotel = value; }
    }
}
