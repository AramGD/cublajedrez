﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesClubAjedrez
{
    public class Campeonato
    {
        private string idCampeonato;
        private string nombre;
        private string tipo;
        private string Participante_nSocio;

        public string IdCampeonato { get => idCampeonato; set => idCampeonato = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Tipo { get => tipo; set => tipo = value; }
        public string Participante_nSocio1 { get => Participante_nSocio; set => Participante_nSocio = value; }
    }
}
