﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesClubAjedrez
{
    public class Hotel
    {
        private string _idHotel;
        private string _nombre;
        private string _direccion;
        private string _telefono;

        public string IdHotel { get => _idHotel; set => _idHotel = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public string Direccion { get => _direccion; set => _direccion = value; }
        public string Telefono { get => _telefono; set => _telefono = value; }
    }
}
