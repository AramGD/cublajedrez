﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesClubAjedrez
{
    public class Movimiento
    {
        private string idMovimiento;
        private string Movimientos;
        private string comentario;
        private string Partida_idPartida;

        public string IdMovimiento { get => idMovimiento; set => idMovimiento = value; }
        public string Movimientos1 { get => Movimientos; set => Movimientos = value; }
        public string Comentario { get => comentario; set => comentario = value; }
        public string Partida_idPartida1 { get => Partida_idPartida; set => Partida_idPartida = value; }
    }
}
